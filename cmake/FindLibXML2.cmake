# - Try to find LibXML 2
# Once done, this will define
#
#  LibXML2_FOUND - system has LibXML2
#  LibXML2_INCLUDE_DIRS - the LibXML2 include directories
#  LibXML2_LIBRARIES - link these to use LibXML2

include(LibFindMacros)

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(LibXML2_PKGCONF libxml-2.0)

# Main include dir
find_path(LibXML2_INCLUDE_DIR
  NAMES xml2 libxml/xpath.h
  HINTS /usr/include/
  PATHS ${LibXML2_PKGCONF_INCLUDE_DIRS}
  PATH_SUFFIXES libxml2
)

# Glib-related libraries also use a separate config header, which is in lib dir
find_path(LibXML2Config_INCLUDE_DIR
  NAMES xml2-config
  HINTS /usr/bin/
  PATHS ${LibXML2_PKGCONF_INCLUDE_DIRS} /usr
  PATH_SUFFIXES include/libxml2/include
)

# Finally the library itself
find_library(LibXML2_LIBRARY
  NAMES xml2
  HINTS /usr/lib64/
  PATHS ${LibXML2_PKGCONF_LIBRARY_DIRS}
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(LibXML2_PROCESS_INCLUDES ${LibXML2_INCLUDE_DIR} ${LibXML2Config_INCLUDE_DIR})
set(LibXML2_PROCESS_LIBS ${LibXML2_LIBRARY})
libfind_process(LibXML2)

