#include <iostream>
#include <string>
#include <list>

#include "GraphGenerator/Factories/GraphFactory.h"
#include "GraphGenerator/IOFiles/KMLWriter.h"
#include "Graph/Graph.h"
#include "Graph/Node.h"
#include "Graph/Label.h"

#include "gdal/ogrsf_frmts.h"
#include "gdal/gdal_priv.h"

int main(int argc, char const *argv[])
{
	SimpleLinker *l = new SimpleLinker("","",OBSTACLE);
	//std::string path = "/home/tguyet/Recherche/Stages/2015/Frederic/trunk/data/shapeFiles/Oscol_2000/Ocsol_test.shp";
	std::string path = "../../data/shapeFiles/Oscol_2000/Ocsol_test.shp";
	const FileWrapper *file = new FileWrapper(path,l);
	KMLWriter * kmlw = new KMLWriter("test", 1, LambertZoneII);
	kmlw->Init();
	/* code */
	GraphFactory * gf = new GraphFactory();

	gf->FetchLayer(*file);

	delete(gf);
	std::cout << "finMain\n" << std::endl;
return 0;
}
