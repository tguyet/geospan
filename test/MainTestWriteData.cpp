/*
 * MainWriteData.cpp
 *
 *  Created on: 30 juin 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 */

#include <iostream>
#include <sstream>
#include <string>

#include "../src/IOFiles/KMLWritter.h"

int main() {
	getStremFileName() << "<?xml version='1.0' encoding='utf-8'?>\n";
	getStremFileName() << "<kml xmlns='http://w...content-available-to-author-only...s.net/kml/2.2'>\n";

	getStremFileName() << "<Placemark>\n"
			<< "<name>" << getFileName() << "</name>\n"
			<< "<version>" << getIdVersion() << "</version>\n"
			<< "<EPSG>" << getEpsg() << "</EPSG> \n"
			<< "<description>This is the path between the 2 points</description>\n"
			<< "<styleUrl>#pathstyle</styleUrl>\n"
			<< "<LineString>\n"
			<< "<tessellate>1</tessellate>\n"
			<< "<coordinates>"
			<< long1 << "," << lat1 << ",0"
			<< " "
			<< long2 << "," << lat2 << ",0"
			<< "</coordinates>\n"
			<< "</LineString>\n"
			<< "</Placemark>\n";


	std::cout << "</kml>\n";

	return 0;
}
