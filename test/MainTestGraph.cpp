/*
 * MainTestGraph.cpp
 *
 *  Created on: 23 juin 2015
 *     Author : Marec Frederic (ddmarec@gmail.com)
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <libxml++/libxml++.h>
#include <libxml++/parsers/textreader.h>

#include <iostream>
#include <stdlib.h>

#include "../src/GraphGenerator/IOFiles/KMLReader.h"



int main()
{
  KMLReader * rfkml  = new KMLReader();
  

  // Set the global C and C++ locale to the user-configured locale,
  // so we can use std::cout with UTF-8, via Glib::ustring, without exceptions.
  std::locale::global(std::locale(""));

  rfkml->KMLReader::inputKMLFile();

  try
  {
    xmlpp::TextReader reader(rfkml->getFileName());

    while(reader.read())
    {
      rfkml->setDepth(reader.get_depth());
      std::cout << KMLReader(rfkml->getDepth()) << "--- node ---" << std::endl;
      std::cout << KMLReader(rfkml->getDepth()) << "name: " << reader.get_name() << std::endl;
      std::cout << KMLReader(rfkml->getDepth()) << "read_outer_xml: " << reader. read_outer_xml() << std::endl;
      std::cout << KMLReader(rfkml->getDepth()) << "depth: " << reader.get_depth() << std::endl;

      if(reader.has_attributes())
      {
        std::cout << KMLReader(rfkml->getDepth()) << "attributes: " << std::endl;
        reader.move_to_first_attribute();
        do
        {
          std::cout << KMLReader(rfkml->getDepth()) << "  " << reader.get_name() << ": " << reader.get_value() << std::endl;
        } while(reader.move_to_next_attribute());
        reader.move_to_element();
      }
      else
      {
        std::cout << KMLReader(rfkml->getDepth()) << "no attributes" << std::endl;
      }

      if(reader.has_value()){
        std::cout << KMLReader(rfkml->getDepth()) << "value:"<< reader.get_value() << "'" << std::endl;
        
      }
      else
        std::cout << KMLReader(rfkml->getDepth()) << "novalue" << std::endl;

    }
  }
  catch(const std::exception& e)
  {
    std::cerr << "Exception caught: " << e.what() << std::endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
};
