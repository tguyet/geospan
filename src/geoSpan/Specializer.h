/*
 * Specializer.h
 *
 *  Created on: 26 juil. 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#ifndef SPECIALIZER_H_
#define SPECIALIZER_H_


class Graph;
class Pattern;
class DFSCode;
class MinerParameters;

#include <list>

using namespace std;

/*
 *
 */
class Specializer {
private:

	const MinerParameters* parameters;


	const Graph* database;

public:
	Specializer();
	Specializer(MinerParameters& param, const Graph* db);
	virtual ~Specializer();

	list<DFSCode*>* getSPecializedPatterns(const DFSCode& pattern)const;
};

#endif /* SPECIALIZER_H_ */
