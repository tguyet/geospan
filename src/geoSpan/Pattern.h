/*
 * Pattern.h
 *
 *  Created on: 29 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#ifndef PATTERN_H_
#define PATTERN_H_

class Graph;
class DFSCode;

#include <string.h>
#include <sstream>
#include <iostream>

using namespace std;
/**
 * Represent a pattern mined from a graph.
 * The pattern is stored as a graph.
 * It also stores the pattern's support and cover in the mined big graph.
 * Patterns are created from a DFSCode.
 * The pattern's graph's nodes are indexed following the DFSCode it was created from.
 *
 * @see Graph
 * @see DFSCode
 */
class Pattern {
private:
	/** The graph representing the pattern. It's node indexes follow the DFSCode used to create this pattern. */
	Graph* patternGraph;

	/** Pattern's support in the mined graph. */
	double support;

	/** Pattern's cover in the mined graph. */
	double cover;

public:

	/**
	 * Construct an empty pattern, with an empty graph.
	 */
	Pattern(const Graph *minedgraph);

	/**
	 * Construct a pattern from a given DFSCode.
	 *
	 * @param code The DFSCode to construct the pattern from.
	 */
	Pattern(const DFSCode& code, const Graph *minedgraph);

	/**
	 * Delete patternGraph.
	 */
	virtual ~Pattern();

	/**
	 * Return the graph representing this pattern.
	 *
	 * @return a reference to the graph representing this pattern.
	 */
	Graph& getPatternGraph() const {return *patternGraph;}

	/**
	 * Static method to create the graph representing a DFSCode.
	 *
	 * @return A pointer to the created graph.
	 */
	static Graph* findPatternGraph(const DFSCode& code, const Graph *minedgraph);

	/**
	 * Returns the pattern's support in the mined graph.
	 *@return A double representing the pattern's support in the mined graph.
	 */
	double getSupport() const {return support;}

	/**
	 * Returns the pattern's cover in the mined graph.
	 *@return A double representing the pattern's cover in the mined graph.
	 */
	double getCover() const {return cover;}

	/**
	 * Returns true if the pattern is a cycle
	 * @return True if the pattern is a cycle.
	 */
	bool isCyclic() const;

	/**
	 * Returns true if the pattern is a tree
	 * @return True if the pattern is a tree.
	 */
	bool isTree() const;

	string toString() const;

	void print(ostream& o) const;

	/**
	 * Read a pattern from a file
	 * @param source Path to the file containing the pattern to read.
	 *
	 * @return A pointer to the pattern read.
	 */
	static Pattern* read(const string& source);

	/**
	 * Compare two patterns by their graph and support.
	 *
	 * @return true if this pattern and the other one have the same support and equivalent graphs.
	 */
	bool operator==(const Pattern& other)const;

	/**
	 *
	 */
	string writePatternGraph() const;
};


ostream& operator<<(ostream& o, const Pattern& p);

#endif /* PATTERN_H_ */
