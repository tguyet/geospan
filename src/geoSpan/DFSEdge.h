/*
 * DFSEdge.h
 *
 *  Created on: 25 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#ifndef DFSEDGE_H_
#define DFSEDGE_H_

class Label;
class Edge;

#include <string.h>
#include <iostream>
#include <sstream>
#include <vector>

using namespace std;
/**
 * Represents an edge in a DFSCode. See "gSpan: graph-based substructure patter mining" by
 * Ya and Han.
 *
 * @see DFSCode
 */
class DFSEdge {
public:
	vector<float> mean_attributes;//test quanti TG
private:
	/**
	 * Index in the code of the first node of this DFSEdge.
	 */
	int nodeAIndex;

	/**
	 * Index in the code of the second node of this DFSEdge.
	 */
	int nodeBIndex;

	/**
	 * Const pointer to this DFSEdge's first node's label.
	 */
	const Label* nodeALabel;

	/**
	 * Const pointer to this DFSEdge's second node's label.
	 */
	const Label* nodeBLabel;

	/**
	 * Const pointer to this DFSEdge's label.
	 */
	const Label* edgeLabel;

	/**
	 * Creates an Edge equivalent to this DFSEdge.
	 * Also creates the two nodes of the edge.
	 *
	 * @return A pointer to an edge equivalent to this DFSEdge.
	 */
	Edge* toEdge()const;

public:
	/**
	 * Empty constructor.
	 */
	DFSEdge();

	/**
	 * Construct a DFSEdge with the given arguments.
	 * The 3 pointer to labels are not directly used,
	 * new labels are created by copying the labels pointed to.
	 */
	DFSEdge(int na, int nb, const Label* la, const Label* lb, const Label* le);

	/**
	 * Copy constructor. No pointer is shared.
	 */
	DFSEdge(const DFSEdge& other);

	/**
	 * Destructor. Destroy the labels.
	 */
	virtual ~DFSEdge();

	/**
	 * Compare this DFSEdge to another one.
	 * The comparison only makes sense to compare 2 DFSEdge that are
	 * at the same index in a DFSCode.
	 * The comparison is defined in the article.
	 *
	 * @param edge A const reference to the DFSEdge to compare to this.
	 *
	 * @return A negative float if this < edge, a positive float if this > edge or
	 * 0 if this == edge
	 */
	float compareTo(const DFSEdge& edge)const;

	/**
	 * Returns the index of this DFSEdge's first node.
	 *
	 * @return The index of this DFSEdge's first node.
	 */
	int getNodeAIndex() const {return nodeAIndex;}

	/**
	 * Returns the index of this DFSEdge's second node.
	 *
	 * @return The index of this DFSEdge's second node.
	 */
	int getNodeBIndex() const {return nodeBIndex;}

	/**
	 * Returns the label of this DFSEdge's first node.
	 *
	 * @return A const reference to the label of this DFSEdge's first node.
	 */
	const Label& getNodeALabel() const {return *nodeALabel;}

	/**
	 * Returns the label of this DFSEdge's second node.
	 *
	 * @return A const reference to the label of this DFSEdge's second node.
	 */
	const Label& getNodeBLabel() const {return *nodeBLabel;	}

	/**
	 * Returns the label of this DFSEdge.
	 *
	 * @return A const reference to the label of this DFSEdge.
	 */
	const Label& getEdgeLabel() const {return *edgeLabel;}


	/**
	 *  Returns true if this DFSEdge is forward,
	 *  meaning it defines an edge adding a node in the code.
	 *
	 *  @return true if this edge is forward.
	 */
	bool isForward()const{return nodeAIndex<nodeBIndex;}

	/**
	 * Affectation operator. No pointer is shared.
	 *
	 */
	DFSEdge& operator=(const DFSEdge& o);

	bool operator==(const DFSEdge& o) const{ return this->compareTo(o)==0;};

	bool operator< (const DFSEdge& o) const{ return this->compareTo(o)<0;};

	bool operator> (const DFSEdge& o) const{ return this->compareTo(o)>0;};

	bool operator>= (const DFSEdge& o) const{ return this->compareTo(o)>=0;};

	bool operator<= (const DFSEdge& o) const{ return this->compareTo(o)<=0;};

	/**
	 * Returns true if o is a valid DFSedge to follow this one in a DFSCode.
	 * Uses rules defined in the article.
	 *
	 * @return true if o is a valid DFSedge to follow this one in a DFSCode.
	 */
	bool isGoodNextNeighbourgh(const DFSEdge& o)const;

	/**
	 * Compares this DFSEdge with an edge.
	 * It compares the result of this->toEdge() to the given edge.
	 *
	 * @param o The Edge to compare to this DFSEdge.
	 *
	 * @return True is this DFSEdge is equivalent to the given edge.
	 */
	bool operator== (const Edge& o) const;

	/**
	 * Compares this DFSEdge with an edge.
	 * It compares the result of this->toEdge() to the given edge.
	 *
	 * @param o The Edge to compare to this DFSEdge.
	 *
	 * @return True is this DFSEdge is not equivalent to the given edge.
	 */
	bool operator!= (const Edge& o) const;

	string toString() const;

	void print(ostream& o) const;

};

void testDFSEdge();

ostream& operator<<(ostream& o, const DFSEdge& e);

#endif /* DFSEDGE_H_ */
