/*
 * Embedding.cpp
 *
 *  Created on: 29 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#include "Embedding.h"
#include "Graph/Edge.h"
#include "Graph/Node.h"
#include <cassert>

Embedding::Embedding() {
	previous=NULL;
	edgeIndex=0;

	embeddedEdge=NULL;
	sameOrderAsDFSEdge=true;
}

Embedding::Embedding(const Edge* root, bool sameOrder) {
	previous=NULL;
	edgeIndex=0;

	embeddedEdge=root;
	sameOrderAsDFSEdge=sameOrder;
}

Embedding::Embedding(const Embedding& embedding) {
	previous=embedding.previous;
	edgeIndex=embedding.edgeIndex;

	embeddedEdge=embedding.embeddedEdge;
	sameOrderAsDFSEdge=embedding.sameOrderAsDFSEdge;
}

//the edges are managed by Graph, previous should not be touched either
//nothing to do here
Embedding::~Embedding() {
}

Embedding& Embedding::operator =(const Embedding& embedding) {
	if(this!= &embedding){
		previous=embedding.previous;
		edgeIndex=embedding.edgeIndex;

		embeddedEdge=embedding.embeddedEdge;
		sameOrderAsDFSEdge=embedding.sameOrderAsDFSEdge;
	}

	return *this;
}

Embedding* Embedding::extend(const Edge* e, bool sameOrder) const {
	Embedding* next=new Embedding();

	next->previous=this;
	next->edgeIndex=this->edgeIndex+1;
	next->embeddedEdge=e;
	next->sameOrderAsDFSEdge=sameOrder;

	return next;
}

unsigned int Embedding::size() const {
	return edgeIndex+1;
}



set<const Node*,idComparator>* Embedding::getNodes()const{

	set<const Node*,idComparator>* nodes;

	if(edgeIndex==0){
		nodes=new set<const Node*,idComparator>();
	}else{
		nodes=previous->getNodes();
	}

	nodes->insert( embeddedEdge->getNodeA() );
	nodes->insert( embeddedEdge->getNodeB() );

	return nodes;

}

const Edge* Embedding::getEdge(int index) const {

	if( index < 0 ||index > edgeIndex){
		return NULL;
	}

	if(edgeIndex==index){
		return embeddedEdge;
	}else{
		return previous->getEdge(index);
	}

}

void Embedding::getEdgesRec(list<const Edge*>&l, int index) const {
	//if( index<=edgeIndex ){
		if( previous!=NULL )
			previous->getEdgesRec(l,index+1);
		//assert(embeddedEdge!=NULL);
		l.push_back(embeddedEdge);
	//}
}

list<const Edge*> Embedding::getEdges() const {
	list<const Edge*> l;
	getEdgesRec(l, 0);
	return l;
}


bool Embedding::getEdgeOrder(int index) const {

	if(index < 0 ||index > edgeIndex){
		return false;
	}

	if(edgeIndex==index){
		return sameOrderAsDFSEdge;
	}else{
		return previous->getEdgeOrder(index);
	}
}

bool Embedding::containsEdge(const Edge* edge) const {

	if(edge->getID()==embeddedEdge->getID()){
		return true;
	}else{
		if(edgeIndex==0){//only one edge to test, and it was not equal
			return false;
		}else{
			return previous->containsEdge(edge);
		}
	}
}

bool Embedding::containsNode(const Node* node) const {

	if(embeddedEdge->getNodeB()->getID()== node->getID() || embeddedEdge->getNodeA()->getID()== node->getID()){
		return true;
	}else{
		if(edgeIndex==0){//only one edge to test, and it was not equal
			return false;
		}else{
			return previous->containsNode(node);
		}
	}
}

int Embedding::getFirstEdgeIndexFor(const Node* node) const {

	if(edgeIndex>0){
		int firstIndex= previous->getFirstEdgeIndexFor(node);
		if(firstIndex>=0){
			return firstIndex;
		}
	}
	//index == 0 or not found previously
	if(embeddedEdge->getNodeA()->getID()== node->getID() || embeddedEdge->getNodeB()->getID()== node->getID()){
		return edgeIndex;
	}else{
		return -1;
	}
}


int Embedding::getEdgeIndex(const Edge& edge) const {

	if(edgeIndex>0){
		int firstIndex= previous->getEdgeIndex(edge);
		if(firstIndex>=0){
			return firstIndex;
		}
	}
	//index == 0 or not found previously
	if(embeddedEdge->getID()== edge.getID() ){
		return edgeIndex;
	}else{
		return -1;
	}
}

string Embedding::toString() const {
	stringstream s;
	if(edgeIndex>0){
		s<<previous->toString()<<endl;
	}else{
		s<<"-----------"<<endl;
	}
	s<<embeddedEdge<<" : "<<(sameOrderAsDFSEdge?"->":"<-");

	return s.str();
}

void Embedding::print(ostream& o) const {
	o<<toString();
}

ostream& operator <<(ostream& o, const Embedding& e) {
	e.print(o);
	return o;
}

void testEmbedding() {
	cout<<"-----------------"<<endl<<"Test Label"<<endl;
}



