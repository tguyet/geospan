/*
 * DFSCode.h
 *
 *  Created on: 29 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#ifndef DFSCODE_H_
#define DFSCODE_H_

class Edge;
class Embedding;
class DFSEdge;
class Pattern;
class Graph;
class Node;
class DFSCodeComparator;
class MinerParameters;

#include <string.h>
#include <iostream>
#include <set>
#include <vector>
#include <list>
#include <vector>
#include <algorithm>
#include <exception>


using namespace std;
/**
 * A DFSCode represents a Depth-First Search Tree of a graph pattern.
 * This representation of a pattern provides a unique code for isomorphic patterns.
 * In this documentation, code can be read as pattern, or more precisely group of isomorphic pattern.
 * See "gSpan: graph-based substructure patter mining" by
 * Ya and Han.
 *
 * For optimization reasons (to avoid numerous copies), a DFSCode consists of a pointer towards
 * the DFSCode it was grown from, a pointer to the last DFSEdge added to this code and the size of the current code.
 */
class DFSCode {
private:
	/**
	 * The code this one was grown from.
	 */
	const DFSCode* previous;

	/**
	 * The number of DFSEdge in this DFSCode.
	 */
	int size;

	/**
	 * List of all the occurrences of this code in the mined graph.
	 */
	list<Embedding*> embeddings;

	/**
	 * The last DFSEdge added to this code.
	 */
	DFSEdge* currentDFSEdge;

	/**
	 * The support of this code in the mined graph.
	 */
	double support;

	/**
	 * Index to give to the next node added to this code.
	 * Indexes of nodes of a DFSCode are stored in DFSEdges.
	 */
	int nextNodeIndex;

	/**
	 * A pointer to the mined graph. Used to compute the cover a code.
	 */
	const Graph* db;

	/**
	 * Creates 1-edge extensions for this code. Only extension having a plausible chance
	 * of being valid are generated.
	 * @param parameters A pointer to mining parameters that will be used to sort out the
	 * non valid embeddings of created extensions.
	 *
	 * @return A pointer to a set of created DFSCode.
	 *
	 */
	set<DFSCode*,DFSCodeComparator>* getExtensions(const MinerParameters* parameters) const;

	/**
	 * Find the 1-edge extensions that add a node to this code.
	 */
	void findForwardEdges(set<DFSCode*,DFSCodeComparator>& extensions,const MinerParameters* parameters)const;

	/**
	 * Find the 1-edge extensions that don't add a node to this code.
	 */
	void findBackwardEdges(set<DFSCode*,DFSCodeComparator>& extensions,const MinerParameters* parameters) const;

	/**
	 * Add a DFSCode to a set of DFSCode. If the DFSCode to add is already present in the set, it's embedding
	 * will be merged with those of the one in the set.
	 * The non valid embeddings of the DFSCode to add are sorted out here.
	 *
	 * @param extensions Reference to a set of DFSCode where to add a new DFSCode.
	 * @param candidateCode The code to add to extensions.
	 * @param parameters A pointer to mining parameters that will be used to sort out the
	 * non valid embeddings of candidateCode.
	 */
	void addCandidate(set<DFSCode*,DFSCodeComparator>& extensions,DFSCode* candidateCode,const MinerParameters* parameters) const;

	/**
	 * Used to find if a given edge's nodes are in the same order as the next DFSEdge to be added to this code.
	 *
	 * @param edge A const reference to an edge to add to an embedding.
	 * @param originID Id of the node from which edge was discovered.
	 *
	 * @return true if edge's node are in the correct order.
	 */
	bool getEdgeOrder(const Edge* edge, int originID) const;

	/**
	 * Sort out embeddings in this code that aren't valid using parameters.validateEmbedding().
	 * If no embeddings remains after, it will return false;
	 *
	 * @param parameters The mining parameters to use to validate each one of this code's embedding.
	 *
	 * @return True if at least one embbedding remains after sorting out the non valid one, false, otherwise.
	 */
	bool validateEmbeddings(const MinerParameters* parameters);

	/**
	 * Returns the index of the last node added to this code.
	 * @return The index of the last node added to this code.
	 */
	int findRightMostIndex()const;

	/**
	 * Returns the last node added to the given embedding.
	 *
	 * @param embedding An embedding of this code.
	 *
	 * @return A const reference to the last node added to the given embedding.
	 */
	const Node* findRightMostNode(const Embedding* embedding)const;

	/**
	 * Extends this DFSCode with a forward edge (that adds a node to the code).
	 * The previous attribute of the returned DFSCode is set with this.
	 * The new DFSCode has only one embedding, which is the extension of one of this code's embedding.
	 * The new DFSEdge is created using the given edge and index.
	 *
	 * @param embeddingToExtend Const reference to the embedding to extend and add to the new DFSCode.
	 * It is one of this code's embedding.
	 * @param candidateEdge Const pointer to the edge with which extend embeddingToExtend.
	 * @param extendedNode Const reference to the node from which candidateEdge was discovered.
	 * It is present in embedding.
	 * @param extendedNodeIndex Index corresponding to the node extendedNode in the DFScode.
	 *
	 * @return A DFSCode extending this one, containing only one embedding that consists of
	 * embeddingToExtend extended with candidateEdge.
	 */
	DFSCode* extendForward(const Embedding& embeddingToExtend,const Edge* candidateEdge,
			const Node* extendedNode, int extendedNodeIndex)const;

	/**
	 * Extends this DFSCode with a backward edge (that doesn't add a node to the code but close a cycle).
	 * The previous attribute of the returned DFSCode is set with this.
	 * The new DFSCode has only one embedding, which is the extension of one of this code's embedding.
	 * The new DFSEdge is created using the given edge and indexes.
	 *
	 * @param embeddingToExtend Const reference to the embedding to extend and add to the new DFSCode.
	 * It is one of this code's embedding.
	 * @param candidateBackwardEdge Pointer to the edge with which extend embeddingToExtend.
	 * @param rightMostNode Const reference to the node from which candidateBackwardEdge was discovered.
	 * It is present in embedding.
	 * @param rightMostIndex Index corresponding to the node rightMostIndex in the DFScode.
	 * @param otherNode Const reference to the node to which candidateBackwardEdge leads.
	 * It is present in embedding.
	 * @param otherNodeIndex Index corresponding to the node otherNode in the DFScode.
	 *
	 * @return A DFSCode extending this one, containing only one embedding that consists of
	 * embeddingToExtend extended with candidateEdge.
	 */
	DFSCode* extendBackward(const Embedding& embeddingToExtend, const Edge* candidateBackwardEdge,
			const Node* rightMostNode, int rightMostIndex,
			const Node* otherNode,int otherNodeIndex) const;

	/**
	 * Gives the index in this DFSCode corresponding to a given node in a given embedding.
	 * The indexes are the one used by the DFSEdges.
	 *
	 * @param n Const reference to the node whose index is wanted.
	 * @param emb Const reference to the embedding in which we want n's index.
	 *
	 * @return The index of the node n in the embedding emb.
	 */
	int getNodeIndex(const Node* n,const Embedding* emb)const;


	/**
	 * Insert all embeddings of another DFSCode in this one.
	 * No test are performed in this method. Equality between the two
	 * DFSCode must be insured before hand.
	 *
	 * @param code DFScode whose embedding insert in this one.
	 */
	void mergeEmbeddings(const DFSCode& code);


public:

	/**
	 * Construct an empty code with no previous one.
	 *
	 * @param database A pointer to the mined graph.
	 */
	DFSCode(const Graph* database);

	/**
	 * Construct a DFSCode with one DFSEdge and no previous DFSCode.
	 * An Embedding is created for each Edge given.
	 * The given Edges must be equals, any Edge not equivalent to the others is not considered.
	 * The first DFSEdge of this code is constructed using the given Edges.
	 *
	 * @param edges A const reference to a list of pointer to Edges. All edges must be equals
	 * @param database A pointer to the mined graph.
	 */
	DFSCode(const list<Edge*>& edges,const Graph* database);

	/**
	 * Destructor. Destroys all it's embeddings and its DFSEdge.
	 */
	virtual ~DFSCode();

	/**
	 * Recursive mining method.
	 *
	 * @parameters Const pointer to the mining parameters used.
	 *
	 * @return The number of valid pattern added to the frequentPatterns
	 */
	int call(const MinerParameters* parameters, list<DFSCode*>* frequentPatterns, int level);

	/**
	 * Return whether this DFSCode is canonical or not.
	 * See the article for more detils.
	 *
	 * @return True is this DFSCode is canonical, false otherwise.
	 */
	bool isCanonical() const;

	/**
	 * Gives the number of node in this code.
	 *
	 * @return The number of node in this code.
	 */
	int nbNodes() const;//{return nextNodeIndex;};

	/**
	 * Gives the number of edges in this code.
	 *
	 * @return The number of edges in this code.
	 */
	int getSize()const{return size;}

	/**
	 * Gives this code's support.
	 *
	 * @return This code's support.
	 */

	double getSupport() const {return support;}

	/**
	 * Returns the formal definition off this code in the form of a vector of DFSEdge, in their order of creation.
	 *
	 * @return A pointer to a vector of pointer to DFSEdge.
	 */
	vector<DFSEdge*>* getCode() const;

	/**
	 * Returns a specific DFSEdge in this code.
	 *
	 * @param index The index of the wanted DFSEdge.
	 *
	 * @return The wanted DFSEdge or nullptr if index is out of bound.
	 */
	const DFSEdge* getDFSEdge(int index) const;

	const DFSEdge* getCurrentDFSEdge() const {return currentDFSEdge;};

	/**
	 * Returns the list of all the embeddings.
	 *
	 * @return A const reference to a list of pointer to the embeddings.
	 */
	const list<Embedding*>& getEmbeddings() const {return embeddings;}

	/**
	 * Compares another DFSCode to this one.
	 * Only the DFSEdges are compared.
	 *
	 * @param other A const reference to the DFSCode to compare to this one.
	 *
	 * @return A negative float if this < other, a positive float if this> other, 0 if this == other
	 */
	float compareTo(const DFSCode& other)const;

	bool operator <(const DFSCode& o)const{return this->compareTo(o)<0;}

	/**
	 * Returns a Pattern object representing this DFSCode.
	 *
	 * @return A pointer to the created Pattern.
	 */
	Pattern* getPattern() const;

	/**
	 * Returns the cover of this code in the mined graph.
	 * It correspond to the percentage of edges in the graph
	 *  found in an embedding of this code
	 *
	 *  @return The percentage of edges in the mined graph covered by this code.
	 */
	double getCover() const;

	string toString() const;

	void print(ostream& o) const;

	static void testDFSCode();

};

class DFSCodeComparator{
public:
	DFSCodeComparator(){}
	bool operator ()(const DFSCode* o1,const DFSCode* o2){return *o1<*o2;}
};

ostream& operator<<(ostream& o, const DFSCode& c);
#endif /* DFSCODE_H_ */
