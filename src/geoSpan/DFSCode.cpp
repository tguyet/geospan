/*
 * DFSCode.cpp
 *
 *  Created on: 29 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#include "DFSCode.h"
#include "Embedding.h"
#include "DFSEdge.h"
#include "MinerParameters.h"
#include "Pattern.h"
#include "Graph/Label.h"
#include "Graph/Edge.h"
#include "Graph/Node.h"
#include "Graph/Graph.h"

#include "config.h"

#include <algorithm>
#include <cstdlib>
#include <cassert>
#include <cfloat>
#include <cmath>

#define CLUSTERING_QUANTITATIVE 0 //active l'utilisation du clustering

#define clearSet(x) while(!x->empty()){delete *x->begin();x->erase(x->begin());}
#define clearList(x) while(!x.empty())delete x.front(),x.pop_front();

DFSCode::DFSCode(const Graph* database) {
	previous=NULL;
	size=1;
	embeddings=list<Embedding*>();
	currentDFSEdge=NULL;
	support=0;
	nextNodeIndex=0;
	db=database;
}

DFSCode::DFSCode(const list<Edge*>& edges,const Graph* database) {
	previous=NULL;
	size=1;
	support=0;
	embeddings=list<Embedding*>();
	nextNodeIndex=2;
	db=database;

	for(const Edge* edge:edges){
		if( *edge==*(edges.front()) ){
			//the first edge is always in the right order, as it has been sorted, and the first DFSEdge is sorted too
			embeddings.push_back(new Embedding(edge,true));
		}else{
			cerr<<"bad edge value when creating a DFSCode embedding"<<endl;
		}
	}

	//if the root is symetric, add the symetric embedded edge to the embeddings
	//this way both nodes of each edge will be nodeA and nodeB from the corresponding DFSEdge
	if(edges.front()->getNodeA()->getLabels()==edges.front()->getNodeB()->getLabels()){
		for(const Edge* edge:edges){

			if(*edge==*(edges.front())){

				//the first edge is now in the wrong order
				embeddings.push_back(new Embedding(edge,false));
			}else{
				cerr<<"bad edge value when creating a DFSCode embedding"<<endl;
			}
		}
	}

	//the  edges has been sorted and are in already in the correct order.
	//if it is symmetric then it doesn't matter which node is A and which is B(same labels)
	currentDFSEdge=new DFSEdge(0, 1,
			edges.front()->getNodeA()->getLabels(),
			edges.front()->getNodeB()->getLabels(),
			edges.front()->getLabels()
	);
}


DFSCode::~DFSCode() {
	clearList(embeddings);
	delete currentDFSEdge;
}


int DFSCode::nbNodes() const {
	return nextNodeIndex;
}


/**
 * Copy a pattern !!
 */
Pattern* DFSCode::getPattern() const {
	return new Pattern(*this,db);
}

set<DFSCode*,DFSCodeComparator>* DFSCode::getExtensions(const MinerParameters* parameters) const {

	set<DFSCode*,DFSCodeComparator>* extensions=new set<DFSCode*,DFSCodeComparator>();

	findBackwardEdges(*extensions,parameters);

	findForwardEdges(*extensions,parameters);

	return extensions;
}

void DFSCode::findForwardEdges(set<DFSCode*,DFSCodeComparator>& extensions,const MinerParameters* parameters) const {

	//try to extend the rightmost path
	int previousRightMost=-1;

	int i;
	vector<DFSEdge*>::const_reverse_iterator dfsEdge;
	const vector<DFSEdge*>* code=getCode();
	for(dfsEdge=code->rbegin() , i=size-1 ; dfsEdge!=code->rend() ; dfsEdge++ , i--){
		//the right most path is made of only forward edges
		if((**dfsEdge).isForward()){
			//test if on the right most path
			if(previousRightMost==-1){//init previousRightMost,
				//and we are on the last node of the right most path
				previousRightMost=(**dfsEdge).getNodeAIndex();
			}else{
				if((**dfsEdge).getNodeBIndex()!=previousRightMost){//if not on the right most path
					continue;
				}else{
					previousRightMost=(**dfsEdge).getNodeAIndex();
				}
			}


			for(Embedding* emb:embeddings){
				const Edge* e=emb->getEdge(i);

				//get node B
				const Node *toExtend=(emb->getEdgeOrder(i)?e->getNodeB():e->getNodeA());

				for(Edge* candidateEdge:toExtend->getEdges()){
					//if candidateEdge brings a new node
					if(!emb->containsNode( candidateEdge->getOtherNode( toExtend ) ) ){
						DFSCode* candidateCode=extendForward(
								*emb,
								candidateEdge,
								toExtend,
								(**dfsEdge).getNodeBIndex());

						addCandidate(extensions, candidateCode, parameters);

						/*/test quanti TG
						cout << "double"<<endl;
						candidateCode=extendForward(
								*emb,
								candidateEdge,
								toExtend,
								(**dfsEdge).getNodeBIndex());

						candidateCode->val=1;

						addCandidate(extensions, candidateCode, parameters);
						//TG: end test quanti TG */
					}
				}
			}
		}
	}



	//try to extend the root node of the code
	for(const Embedding* emb:embeddings){

		//get first node, test in case the first edge is symetric and not in order
		const Node* toExtend=(emb->getEdgeOrder(0)?emb->getEdge(0)->getNodeA():emb->getEdge(0)->getNodeB());

		for(Edge* candidateEdge:toExtend->getEdges()){

			//if candidateEdge brings a new node
			if(!emb->containsNode( candidateEdge->getOtherNode( toExtend ) ) ){
				DFSCode* candidateCode=this->extendForward(
						*emb,
						candidateEdge,
						toExtend,
						getDFSEdge(0)->getNodeAIndex());

				addCandidate(extensions, candidateCode,parameters);

				/*/TG: test
				cout << "double"<<endl;
				candidateCode=extendForward(
						*emb,
						candidateEdge,
						toExtend,
						getDFSEdge(0)->getNodeAIndex());

				candidateCode->val=1;

				addCandidate(extensions, candidateCode, parameters);
				//TG: end test*/
			}
		}
	}

	delete code;
}


void DFSCode::findBackwardEdges(set<DFSCode*,DFSCodeComparator>& extensions,const MinerParameters* parameters) const {

	int rightMostIndex=findRightMostIndex();

	for(const Embedding* emb:embeddings){

		const Node* rightMostNode=findRightMostNode(emb);

		//find all backward extensions for this new node
		//for all possible edge from rightMostNode
		for( const Edge* candidateBackwardEdge:rightMostNode->getEdges() ){

			//only new edges for this embedding are considered
			if(emb->containsEdge( candidateBackwardEdge ) ) {
				continue;
			}

			const Node* otherNode=candidateBackwardEdge->getOtherNode(rightMostNode);

			//if otherNode is in embedding :  backward edge
			if(emb->containsNode(otherNode)){

				DFSCode* candidateCode=extendBackward(
						*emb,
						candidateBackwardEdge,
						rightMostNode,
						rightMostIndex,
						otherNode,
						getNodeIndex(otherNode,emb));

				addCandidate(extensions, candidateCode,parameters);
			}
		}
	}

}

void DFSCode::addCandidate(set<DFSCode*,DFSCodeComparator>& extensions, DFSCode* candidateCode,const MinerParameters* parameters ) const {
	//try to add the candidate code to extensions
	//if already present, merge the embeddings and delete the candidate

	if(parameters!=NULL) {
		if(!candidateCode->validateEmbeddings(parameters)) {//validate removed all embeddings for this code
			delete candidateCode;
			return;
		}
	}

	//cout << "avant " << extensions.size() <<endl;
	pair<set<DFSCode*>::iterator,bool> res=extensions.insert(candidateCode);
	//cout << "apres " << extensions.size() <<endl;
	if(!res.second){
		(*res.first)->mergeEmbeddings(*candidateCode);

		//forget the embeddings just shared so they are not deleted
		candidateCode->embeddings.clear();
		delete candidateCode;
	}
}


bool DFSCode::validateEmbeddings(const MinerParameters* parameters) {

	list<Embedding*>::iterator embedding=embeddings.begin();

	while(embedding!=embeddings.end()){
		if(!parameters->validateEmbedding(*embedding)){
			delete *embedding;
			embedding=embeddings.erase(embedding);
		}else{
			embedding++;
		}
	}

	return embeddings.size()>0;
}

bool DFSCode::getEdgeOrder(const Edge* edge, int originID) const {
	return edge->getNodeA()->getID()==originID;
}

int DFSCode::findRightMostIndex() const {

	return currentDFSEdge->isForward()?currentDFSEdge->getNodeBIndex():currentDFSEdge->getNodeAIndex();
}

const Node* DFSCode::findRightMostNode(const Embedding* embedding) const {
	const Edge* lastEmbeddedEdge= embedding->getEdge(size-1);
	bool rightOrder=embedding->getEdgeOrder(size-1);

	if(currentDFSEdge->isForward()){
		return rightOrder?lastEmbeddedEdge->getNodeB():lastEmbeddedEdge->getNodeA();
	}else{
		return rightOrder?lastEmbeddedEdge->getNodeA():lastEmbeddedEdge->getNodeB();
	}
}

DFSCode* DFSCode::extendForward( const Embedding& embeddingToExtend, const Edge* candidateEdge,
		const Node* toExtend, int toExtendIndex) const {

	DFSCode* next=new DFSCode(db);

	next->previous=this;
	next->size=size+1;
	next->support=support;


	const Node* newNode=candidateEdge->getOtherNode( toExtend );
	DFSEdge* nextDFSEdge = new DFSEdge(
			toExtendIndex,
			nextNodeIndex,
			toExtend->getLabels(),
			newNode->getLabels(),
			candidateEdge->getLabels());

	next->nextNodeIndex=nextNodeIndex+1;

	Embedding* candidateEmbedding=embeddingToExtend.extend(candidateEdge, getEdgeOrder(candidateEdge, toExtend->getID()));

	next->embeddings.push_back(candidateEmbedding);
	next->currentDFSEdge=nextDFSEdge;

	return next;
}

DFSCode* DFSCode::extendBackward(const Embedding& embeddingToExtend, const Edge* candidateBackwardEdge,
		const Node* rightMostNode, int rightMostIndex,
		const Node* otherNode,int otherNodeIndex)const {

	DFSCode* next=new DFSCode(db);

	next->previous=this;
	next->size=size+1;
	next->nextNodeIndex=nextNodeIndex;
	next->support=support;


	DFSEdge* nextDFSEdge = new DFSEdge(
			rightMostIndex,
			otherNodeIndex,
			rightMostNode->getLabels(),
			otherNode->getLabels(),
			candidateBackwardEdge->getLabels());


	Embedding* candidateEmbedding=embeddingToExtend.extend(candidateBackwardEdge,getEdgeOrder(candidateBackwardEdge, rightMostNode->getID()));

	next->embeddings.push_back(candidateEmbedding);
	next->currentDFSEdge=nextDFSEdge;

	return next;
}

int DFSCode::getNodeIndex(const Node* n,const Embedding* emb) const {
	//the first edge a node appears on, it will always be as node B,
	//except if it is the first edge of the code
	int edgeIndex=emb->getFirstEdgeIndexFor(n);

	const DFSEdge* dfsEdge=getDFSEdge(edgeIndex);
	const Edge* embeddedEdge=emb->getEdge(edgeIndex);

	//if first edge, then test if node A or B (in case of symmetry amongst other things),
	//otherwise, always node B
	if(edgeIndex==0){
		//it works
		return(n->getID()==embeddedEdge->getNodeA()->getID())==(emb->getEdgeOrder(edgeIndex))?0:1;
	} else {
		return dfsEdge->getNodeBIndex();
	}
}

void DFSCode::mergeEmbeddings(const DFSCode& code) {
	//no tests, equivalence between codes is given by the set
	this->embeddings.insert(this->embeddings.end(),code.embeddings.begin(),code.embeddings.end());
}


/**
 * Created on 2005-04-12 by
 * - Roger Zhang (rogerz@cs.dal.ca)
 */
int *k_means(double **data, int n, int m, int k, double t, double **centroids, double *error)
{
   /* output cluster label for each data point */
   int *labels = (int*)calloc(n, sizeof(int));

   int h, i, j; /* loop counters, of course :) */
   int *counts = (int*)calloc(k, sizeof(int)); /* size of each cluster */
   double old_error; /* sum of squared euclidean distance */
   double **c = centroids ? centroids : (double**)calloc(k, sizeof(double*));
   double **c1 = (double**)calloc(k, sizeof(double*)); /* temp centroids */
   *error = DBL_MAX;

   assert(data && k > 0 && k <= n && m > 0 && t >= 0); /* for debugging */

   /****
   ** initialization */

   for (h = i = 0; i < k; h += n / k, i++) {
      c1[i] = (double*)calloc(m, sizeof(double));
      if (!centroids) {
         c[i] = (double*)calloc(m, sizeof(double));
      }
      /* pick k points as initial centroids */
      for (j = m; j-- > 0; c[i][j] = data[h][j]);
   }

   /****
   ** main loop */

   do {
      /* save error from last step */
      old_error = *error, *error = 0;

      /* clear old counts and temp centroids */
      for (i = 0; i < k; counts[i++] = 0) {
         for (j = 0; j < m; c1[i][j++] = 0);
      }

      for (h = 0; h < n; h++) {
         /* identify the closest cluster */
         double min_distance = DBL_MAX;
         for (i = 0; i < k; i++) {
            double distance = 0;
            for (j = m; j-- > 0; distance += pow(data[h][j] - c[i][j], 2));
            if (distance < min_distance) {
               labels[h] = i;
               min_distance = distance;
            }
         }
         /* update size and temp centroid of the destination cluster */
         for (j = m; j-- > 0; c1[labels[h]][j] += data[h][j]);
         counts[labels[h]]++;
         /* update standard error */
         *error += min_distance;
      }

      for (i = 0; i < k; i++) { /* update all centroids */
         for (j = 0; j < m; j++) {
            c[i][j] = counts[i] ? c1[i][j] / counts[i] : c1[i][j];
         }
      }

   } while (fabs(*error - old_error) > t);

   /****
   ** housekeeping */

   for (i = 0; i < k; i++) {
      if (!centroids) {
         free(c[i]);
      }
      free(c1[i]);
   }

   if (!centroids) {
      free(c);
   }
   free(c1);
   free(counts);

   return labels;
}


/**
 * \brief Cette méthode est le coeur de gSpan : elle implémente le schéma principal de l'algorithme.
 *
 * La fonction ajoute le code à la liste des motifs fréquents si il est fréquent (ou s'il existe déjà)
 *
 * \param parameters Contient les fonctions spécifiques à uen adapation de gSpan : la fonction de calcul de support,
 * la fonction de calcul du support (e.g. avec contraintes spatiales)
 * \param frequentPatterns structure de données dans laquelle sont accumulés les motifs fréquents
 * \param level Niveau de récursion
 * \param output : if output is not NULL, then frequent pattern are provided online on this stream
 * \return Retourne le nombre de motifs contenant ce motif ayant été généré par le code DFS courant
 */
int DFSCode::call(const MinerParameters* parameters, list<DFSCode*>* frequentPatterns, int level) {
	int nb=0;

	if( parameters->getMaxDepth() ) {
		if( level >= (int)parameters->getMaxDepth() ) {
			return nb;
		}
	}

	//test canonical
	if( isCanonical() ){
#if !defined(NDEBUG) and DEBUG_LEVEL>=4
		for(int i=0;i<level;i++) cout << "  ";
		cout << "-> is canonical" << endl;
#endif

#if CLUSTERING_QUANTITATIVE
		//* KMEANS
		list<vector<float> > data;
		for( Embedding* emb:getEmbeddings() ){
			list<const Edge*> edges=emb->getEdges();
			for(const Edge *e : edges) {

				if( (*e->getNodeA()->getLabels())==getCurrentDFSEdge()->getNodeALabel()
						&& (*e->getNodeB()->getLabels())==getCurrentDFSEdge()->getNodeBLabel()
						&& (*e->getLabels())==getCurrentDFSEdge()->getEdgeLabel()) {
					float rx=e->getNodeA()->X()-e->getNodeB()->X();
					float ry=e->getNodeA()->Y()-e->getNodeB()->Y();

					vector<float> d(3);
					d[0] = sqrt(rx*rx+ry*ry);
					d[1] = e->getNodeA()->getFltLabel("Surface_m");
					d[2] = e->getNodeB()->getFltLabel("Surface_m");
					data.push_back(d);
				}
			}
		}

		if(data.size()==0) {
			return 0;
		}

		double errormin=DBL_MAX;
		double bicmin=DBL_MAX;
		int argmin=0;
		double **argcentroids=NULL;
		int *argcid=NULL;
		int dim = data.front().size();

		for(unsigned int k=2;k<4;k++) {//TODO : améliorer le critère de séparation
			unsigned int lk=k;
			double **centroids=NULL;
			int *cid=NULL;
			double error=DBL_MAX;
			if(data.size()>lk) {
				//construction of a matrix of values
				double **vdata = (double**)calloc(data.size(), sizeof(double*));
				int i=0,j;
				for(vector<float> m : data) {
					j=0;
					if((unsigned int)dim!=m.size()) cerr << "dimension error" <<endl;
					vdata[i]=(double*)calloc(dim, sizeof(double));
					for(float f : m) {
						vdata[i][j]=f;
						j++;
					}
					i++;
				}

				centroids=(double**)calloc(lk, sizeof(double*));
				for(unsigned int i=0; i<lk; i++)
					centroids[i]=(double*)calloc(dim, sizeof(double));
				cid=k_means(vdata, data.size(), dim, lk, 0.01, centroids, &error);

				//liberation de la mémoire pour la matrice de données
				for(unsigned int i=0; i<data.size(); i++)
					free(vdata[i]);
				free(vdata);
			} else {
				error=0;
				cid=(int*)calloc(data.size(), sizeof(int));
				lk=data.size();
				centroids=(double**)calloc(data.size(), sizeof(double*));
				int i=0,j;
				for(vector<float> m : data) {
					j=0;
					centroids[i]=(double*)calloc(m.size(), sizeof(double));
					for(float f : m) {
						centroids[i][j]=f;
						j++;
					}
					cid[i]=i;
					i++;
				}
			}


			float n=(float)data.size();
			float bic = n*log(error/n) + lk*log(n);

			if( bic<bicmin ) {
				//liberation de la mémoire pour la matrice des anciens centroides
				if(argcentroids) {
					for(int i=0; i<argmin; i++)
						free(argcentroids[i]);
					free(argcentroids);
				}
				if(argcid) free(argcid);
				argcentroids = centroids;
				argcid=cid;
				argmin=lk;
			} else {
				//liberation de la mémoire pour la matrice des centroides
				for(unsigned int i=0; i<lk; i++)
					free(centroids[i]);
				free(centroids);
				free(cid);
			}
		}

		vector<DFSCode *> codes(argmin);
		for(int cl=0; cl<argmin; cl++) {
			DFSCode *c=new DFSCode(*this);
			c->val=cl;
			c->embeddings.clear();
			c->currentDFSEdge = new DFSEdge(*currentDFSEdge); //deep copy of the currentEdge
			c->currentDFSEdge->mean_attributes = vector<float>(dim);
			for(int d=0; d<dim;d++) {
				c->currentDFSEdge->mean_attributes[d]=argcentroids[cl][d];
			}
			codes[cl]=c;
		}

		//tres calculatoire : on reprends tous les embeddings et on regarde la classe qui lui convient le mieux !
		int i=0;
		for( Embedding* emb:getEmbeddings() ){
			list<const Edge*> edges=emb->getEdges();
			double dmin=DBL_MAX;
			int argdmin=0;
			for(const Edge *e : edges) {
				if( (*e->getNodeA()->getLabels())==getCurrentDFSEdge()->getNodeALabel()
						&& (*e->getNodeB()->getLabels())==getCurrentDFSEdge()->getNodeBLabel()
						&& (*e->getLabels())==getCurrentDFSEdge()->getEdgeLabel() ) {
					float rx=e->getNodeA()->X()-e->getNodeB()->X();
					float ry=e->getNodeA()->Y()-e->getNodeB()->Y();
					double de=sqrt(rx*rx+ry*ry);
					for(int cl=0; cl<argmin; cl++) {
						double d=de-argcentroids[cl][0];
						d=sqrt(d*d);
						if(dmin>d) {
							argdmin=cl;
							dmin=d;
						}
					}
				}
			}
			codes[argdmin]->embeddings.push_back(emb);
			i++;
		}

		//recalcul du support
		for(int cl=0; cl<argmin; cl++) {
			codes[cl]->support=parameters->getSupport(*codes[cl]);
		}

		//finalisation de la liberation mémoire
		if(argcentroids) {
			for(int i=0; i<argmin; i++)
				free(argcentroids[i]);
			free(argcentroids);
		}
		if(argcid) free(argcid);

#else //CLUSTERING_QUANTITATIVE
		//Without quantitative clustering intervals (classic)

		//Compute the adaptive support
		support=parameters->getSupport(*this);

		list<DFSCode *> codes;
		codes.push_back(this);
#endif //CLUSTERING_QUANTITATIVE

		for(DFSCode *code:codes) {

	#if !defined(NDEBUG) and DEBUG_LEVEL>=4
			for(int i=0;i<level;i++) cout << "  ";
			cout << "DFSCode: " << code->toString() << endl;
			for(int i=0;i<level;i++) cout << "  ";
			cout << "support: " << code->support << " threshold: " << parameters->getThreshold(*code) << endl;
	#endif
			//test threshold
			if( code->support>=parameters->getThreshold(*code) ){ //good pattern
	#if !defined(NDEBUG) and DEBUG_LEVEL>=4
				for(int i=0;i<level;i++) cout << "  ";
				cout << "-> is frequent" << endl;
	#endif

				if( parameters->getOutput() ) {
					(*parameters->getOutput()) << "Pattern" << endl;
					code->getPattern()->print(*parameters->getOutput());
					(*parameters->getOutput()) << "============" << endl;
				}

				//add this to frequentPatterns
				frequentPatterns->push_back(code);
				nb++;

				//find all possibles and probable extensions in the database
				set<DFSCode*,DFSCodeComparator>* extensions=code->getExtensions(parameters);

				//for each possible extension, call the recursive procedure
				for(DFSCode* extension:*extensions){
					int nbe=extension->call(parameters, frequentPatterns, level+1);

					if(nbe==0) { //it didn't give any valid pattern, it can be deleted
						delete extension;
					}
					nb+=nbe;
				}

				extensions->clear();
				delete extensions;
			}
		}
	}

	return nb;
}

bool DFSCode::isCanonical() const {

	//this is sufficient to detect some non canonical graph thanks to other constraints used in candidates generation
	for(int i=1;i<size;i++){
		if(!getDFSEdge(i-1)->isGoodNextNeighbourgh(*getDFSEdge(i))){
			return false;
		}
	}

	//get the pattern graph, will be modified
	Graph* patternGraph=Pattern::findPatternGraph(*this, db);
	bool res=true;

	//find all possible first edges

	//sort nodes in edges
	for(Edge* edge:patternGraph->getEdges()){
		edge->sortNodesByFreq();
	}
	//sort edges
	patternGraph->sortEdges(PreGSpanEdgeFrequencyComparator());

	//first test, if the first edge of this DFSCode is not the min, we can stop
	if( *getDFSEdge(0)!=*(patternGraph->getEdges().front()) ){
		res=false;
	}

	//Test if the current DFSCode is minimal
	if(res){
		//build all possible codes that are =< to this
		//could be only one code with multiles embeddings
		set<DFSCode*,DFSCodeComparator>* candidates=new set<DFSCode*,DFSCodeComparator>();

		list<Edge*> root=list<Edge*>();
		for(Edge* edge:patternGraph->getEdges()) {
			if( *getDFSEdge(0)==*edge ){
				root.push_back(edge);
			} else {
				break;
			}
		}

		candidates->insert( new DFSCode(root,patternGraph) );

		set<DFSCode*>* toDelete=new set<DFSCode*>();
		for(int i=1; i<this->size && res; i++){
			set<DFSCode*,DFSCodeComparator>* nextCandidates=new set<DFSCode*,DFSCodeComparator>();

			for(DFSCode* candidate:*candidates) {
				set<DFSCode*,DFSCodeComparator>* exts=candidate->getExtensions(NULL);

				DFSCode* extension;
				while(!exts->empty() && res) {

					extension=*exts->begin();

					float compLastDFSEdge=extension->getDFSEdge(i)->compareTo(*this->getDFSEdge(i));
					if(compLastDFSEdge<0){//this DFSCode is not canonical
						res=false;

						clearSet(exts)

						clearSet(nextCandidates)
						delete nextCandidates;
						nextCandidates=NULL;
					} else {
						if(compLastDFSEdge==0){//the extension is a good one

							addCandidate(*nextCandidates,extension,NULL);
						} else {//the extension is a bad one, we don't need it anymore
							delete extension;
						}
						exts->erase( exts->begin() );//faster
						//exts->erase(extension);
					}
				}

				delete exts;
			}

			//to save the embeddings of the DFSCode
			toDelete->insert(candidates->begin(),candidates->end());
			candidates->clear();
			delete candidates;
			candidates=nextCandidates;
		}

		clearSet(toDelete)
		delete toDelete;
		if(candidates!=NULL){
			clearSet(candidates)
			delete candidates;
		}
	}
	delete patternGraph;

	return res;
}

string DFSCode::toString() const {
	stringstream s;
	if(size>1){
		s << previous->toString() << "-";
	}
	s << *currentDFSEdge;
	return s.str();
}


vector<DFSEdge*>* DFSCode::getCode() const{
	vector<DFSEdge*>* code;
	if(previous!=NULL){
		code=previous->getCode();
	}else{
		code=new vector<DFSEdge*>();
	}

	code->push_back(currentDFSEdge);

	return code;
}

const DFSEdge* DFSCode::getDFSEdge(int index) const{
	if(index<0 || index> size-1){
		return NULL;
	}

	if(index==size-1){
		return currentDFSEdge;
	}else{
		return previous->getDFSEdge(index);
	}
}

double DFSCode::getCover() const {

	set<int> edges=set<int>();
	for(Embedding* emb:embeddings){
		for(int i=0;i<getSize();i++){
			edges.insert(emb->getEdge(i)->getID());
		}
	}
	return (double)(100*edges.size())/db->getEdges().size();

}

float DFSCode::compareTo(const DFSCode& other) const {
	int maxIndex=min(this->size, other.size);
	float comp=0;
	int index=0;

	while(comp==0 && index<maxIndex){
		comp=this->getDFSEdge(index)->compareTo(*other.getDFSEdge(index));
		index++;
	}

	//shortest one is inferior if all edges are equals
	if(!comp){
		comp = this->size - other.size;
	}

	return comp;
}

void DFSCode::print(ostream& o) const {
	o<<toString();
}


ostream& operator <<(ostream& o, const DFSCode& c) {
	c.print(o);
	return o;
}


void DFSCode::testDFSCode() {
	/*
	cout<<"-----------------"<<endl<<"Test DFSCode"<<endl;

	DFSCode c1=DFSCode(nullptr);
	DFSCode c2=DFSCode(nullptr);

	c1.currentDFSEdge=new DFSEdge(0,1,new Label("{l=a}"),new Label("{l=a}"),new Label("{le=e}"));
	c1.currentDFSEdge=new DFSEdge(0,2,new Label("{l=a}"),new Label("{l=a}"),new Label("{le=e}"));

	c2.currentDFSEdge=new DFSEdge(0,1,new Label("{l=a}"),new Label("{l=a}"),new Label("{le=e}"));
	c2.currentDFSEdge=new DFSEdge(1,2,new Label("{l=a}"),new Label("{l=a}"),new Label("{le=e}"));



	cout<<"true : "<<true<<endl;
	cout<<"assert true : "<<(c2<c1)<<endl;
	cout<<"assert false : "<<(c1<c2)<<endl;
	cout<<"assert pos : "<<c1.compareTo(c2)<<endl;
	cout<<"assert neg : "<<c2.compareTo(c1)<<endl;
	cout<<"assert 0 : "<<c2.compareTo(c2)<<endl;
	cout<<"assert 0 : "<<c1.compareTo(c1)<<endl;



	cout<<"-----------------"<<endl<<"Test Pattern"<<endl;


	DFSCode cp=DFSCode(nullptr);
	cp.currentDFSEdge->push_back(DFSEdge(0,1,new Label("{l=b}"),new Label("{l=c}"),new Label("{le=bc}")));
	cp.currentDFSEdge->push_back(DFSEdge(1,2,new Label("{l=c}"),new Label("{l=a}"),new Label("{le=ca}")));
	cp.currentDFSEdge->push_back(DFSEdge(2,3,new Label("{l=a}"),new Label("{l=a}"),new Label("{le=aa}")));
	cp.currentDFSEdge->push_back(DFSEdge(3,0,new Label("{l=a}"),new Label("{l=b}"),new Label("{le=ab}")));
	cp.currentDFSEdge->push_back(DFSEdge(3,4,new Label("{l=a}"),new Label("{l=a}"),new Label("{le=aa}")));
	cp.currentDFSEdge->push_back(DFSEdge(4,0,new Label("{l=a}"),new Label("{l=b}"),new Label("{le=ab}")));

	Pattern p=Pattern(cp,false);

	Graph patternGraph=p.getPatternGraph();
	cout<<patternGraph<<endl;

	patternGraph.computeLabelFrequency();
	//sort edges
	for(Edge* edge:*patternGraph.getEdges()){
		edge->sortNodesByFreq();
	}

	patternGraph.getEdges()->sort(PreGSpanEdgeFrequencyComparator());

	cout<<patternGraph<<endl;
	 */
}



