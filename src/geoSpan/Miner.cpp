/*
 * Miner.cpp
 *
 *  Created on: 30 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *      Author: Thomas Guyet
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#include "Miner.h"
#include "Pattern.h"
#include "DFSCode.h"
#include "MinerParameters.h"
#include "Graph/Graph.h"
#include "Graph/Edge.h"
#include "Graph/Node.h"
#include "Graph/Label.h"
#include "Pattern.h"
#include "Embedding.h"
#include "Pattern.h"


#define GENERATE_PATTERNS_NODES 0

#define clearList(x) while(!x.empty())delete x.front(),x.pop_front();
#define clearListP(x) while(!x->empty())delete x->front(),x->pop_front();

Miner::Miner(Graph* db, MinerParameters* param){
	parameters = param;
	database=db;
}

Miner::~Miner() {
	delete database;
}

list<Pattern*>* Miner::mine(bool verbose) {
	list<DFSCode*>* patterns=new list<DFSCode*>();

	//Construction des embeddings pour des motifs qui sont des pairs de noeuds
	list<list<Edge*>*>* embeddings=findRootsEmbeddings();

	if(verbose){
		cout<<"####### Nodes ######"<<endl;
		cout<< *(database->getNodeDictionary()) <<endl;

		cout<<endl<<"####### Edges ######"<<endl;
		cout<< *(database->getEdgeDictionary()) <<endl;
	}

	for(list<Edge*>* embedding:*embeddings){

		if(verbose){
			cout<<"#############"<<endl;
			cout<< *(embedding->front()->getNodeA()->getLabels()) <<" - ";
			cout<< *(embedding->front()->getLabels()) <<" - ";
			cout<< *(embedding->front()->getNodeB()->getLabels()) <<endl;
			cout<<"Starting threshold : "<<parameters->findStart(*embedding->front())<<endl;
			cout<<"Starting support : "<<embedding->size()<<endl;
		}
		DFSCode* code=new DFSCode(*embedding, database);
		//TODO : supprimer correctement de la mémoire !

		list<DFSCode*> patternsFound;
		int nb=code->call(parameters, &patternsFound, 0);

		if(verbose){
			cout<<nb<<" patterns found"<<endl;
		}

		patterns->insert( patterns->end(), patternsFound.begin(), patternsFound.end());

		delete embedding;
		patternsFound.clear();
	}
	embeddings->clear();
	delete embeddings;

	list<Pattern*>* patternsS=new list<Pattern*>();
	std::map<int, std::list<int> > nodespatterns;
	for(DFSCode* code:*patterns){
		Pattern *P=new Pattern(*code, database);
		patternsS->push_back(P);
		//delete code;

		//TG : ajout de l'identification des arcs couverts par un pattern (pour comparaison avec chemins de Peano, EGC 2014)
#if GENERATE_PATTERNS_NODES
		const list<Embedding*> &embs=code->getEmbeddings();
		set<const Edge *> wholeedges;
		set<const Node *> wholenodes;
		for(Embedding* e : embs ) {
			list<const Edge*> le = e->getEdges();
			wholeedges.insert( le.begin(), le.end() );
			for(const Edge*ed: le) {
				wholenodes.insert( ed->getNodeA() );
				wholenodes.insert( ed->getNodeB() );
			}
		}

		cout <<"=================="<<endl;
		cout <<"==== PATTERNS ===="<<endl;
		cout << *P << endl;

		//if( P->getPatternGraph().countNodes()> 4 ) {
		for( const Node *e : wholenodes) {
			nodespatterns[patternsS->size()].push_back( e->getIntLabel("ID_2000") );
		}
		//}
#endif
	}

#if GENERATE_PATTERNS_NODES
	cout <<"===== NODES ====="<<endl;
	for(pair<int, list<int> >p : nodespatterns) {
		cout << p.first<< ": " ;
		for(int pat : p.second) cout << pat <<",";
		cout << endl;
	}
#endif

	clearListP(patterns)
	delete patterns;

	return patternsS;
}


list<list<Edge*> *>* Miner::findRootsEmbeddings() {
	list<list<Edge*>* >* embeddings=new list<list<Edge*> *>();

	sortDBEdges();

	Edge* templateEdge=database->getEdges().front();
	list<Edge*>* embs=new list<Edge*>();

	for(Edge* e:database->getEdges()) {
		//cout << "embeddings :" << *(e->getNodeA()->getLabels()) << *e << *(e->getNodeB()->getLabels()) <<endl;
		if( *e!=*templateEdge ){
			embeddings->push_back(embs);
			templateEdge=e;
			embs=new list<Edge*>();
		}
		embs->push_back(e);
	}

	//last template
	embeddings->push_back(embs);

	return embeddings;
}

void Miner::sortDBEdges() {

	//Each edge is arranged to has the "lower" node in place A and "upper" in place B.
	for(Edge* e:database->getEdges()){
		e->sortNodesByFreq();
	}

	//The list of edge is sorted according to the comparator
	database->sortEdges(PreGSpanEdgeFrequencyComparator());
}

