/*
 * Miner.h
 *
 *  Created on: 30 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#ifndef MINER_H_
#define MINER_H_

class Edge;
class Graph;
class Pattern;
class MinerParameters;

#include <list>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

/**
 * \brief Mine big graph for characteristics patterns using an adaptative threshold.
 */
class Miner {
private:
	/**
	 * MinerParameters used to parametrize the adaptative mining and describe a valid occurrence.
	 */
	MinerParameters* parameters;

	/**
	 * The graph to mine.
	 */
	Graph* database;

	/**
	 * Find the embeddings in the database graph for all the roots (2 nodes) edges.
	 * The embeddings for each root is the list of all it's occurrences in the graph.
	 *
	 * @return A pointer to a list of of pointer to a list of pointer to Edge.
	 */
	list<list<Edge*>*>* findRootsEmbeddings();

	/**
	 * Sort the edge of the database graph using PreGSpanEdgeFrequencyComparator.
	 * First compute the labels frequency in the database graph, then order the
	 * nodes in each of the graph's edge and finally sort the edges.
	 *
	 * @see PreGSpanEdgeFrequencyComparator
	 */
	void sortDBEdges();

public:

	/**
	 * Construct a miner for the graph db that uses the given parameters.
	 */
	Miner(Graph* db, MinerParameters* parameters);

	/**
	 * Delete the database graph
	 */
	virtual ~Miner();


	void setOuput(string filename);

	/**
	 * Mine the database graph for all characteristic patterns.
	 * The database graph is destroyed in the process.
	 * The returned list follows the patterns discovery order.
	 *
	 * @param verbose if true, will print steps while mining
	 *
	 * @return A pointer to a list of pointer to the patterns found.
	 */
	list<Pattern*>* mine(bool verbose);
};

#endif /* MINER_H_ */
