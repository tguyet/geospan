/*
 * Embedding.h
 *
 *  Created on: 29 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#ifndef EMBEDDING_H_
#define EMBEDDING_H_

class Edge;
class Node;

#include <string.h>
#include <iostream>
#include <set>

#include "Graph/Graphable.h"

using namespace std;

/**
 * Represent an occurrence of a pattern in the database graph.
 * It contains pointers to edges in the database graph that follows
 * the same indexing than the DFSEdges of the DFSCode this
 * embedding is an occurrence of.
 *
 * For optimization reasons (to avoid numerous copies), an embedding consists of a pointer towards
 * the embedding it was grown from, a pointer to the last edge in the database graph added to
 * this embedding and this edge's index in this embedding.
 */
class Embedding {
private:
	/**
	 * The embedding from this one was grow from.
	 */
	const Embedding* previous;

	/**
	 * The last edge added to this embedding.
	 */
	const Edge* embeddedEdge;

	/**
	 * The index in this embedding of embeddedEdge.
	 */
	short edgeIndex;

	/**
	 * A boolean stating if the nodes of embeddedEdge are in the same order as
	 * the nodes described in the corresponding DFSEdge's.
	 */
	bool sameOrderAsDFSEdge;



public:
	/**
	 * Construct an empty Embedding without a previous one.
	 */
	Embedding();

	/**
	 * Construct an Embedding with one edge, and no previous Embedding.
	 * @param root A pointer to this Embedding first edge
	 * @param sameOrder boolean stating if root's node are
	 * in the same order as the corresponding DFSEdge
	 */
	Embedding(const Edge* root,bool sameOrder);

	/**
	 * Copy constructor.
	 * Copy all the fields.
	 */
	Embedding(const Embedding& embedding);

	/**
	 * Doesn't destroy previous nor embeddedEdge, as they are shared resources, handled at others place.
	 */
	virtual ~Embedding();


	/**
	 * Creates an Embedding that adds an edge to this one.
	 * The created embedding will have this one as previous, embed the edge e,
	 * with an index equals to this.edgeIndex+1.
	 *
	 * @param e The edge to embed.
	 * @param sameOrder Are e's nodes in the same order as the corresponding DFSEdge's.
	 *
	 * @return A pointer to new Embedding extend this one with the edge e.
	 */
	Embedding* extend(const Edge* e,bool sameOrder)const;

	/**
	 * Do the same as the copy constructor.
	 */
	Embedding& operator =(const Embedding& embedding);

	/**
	 * Returns the number of embedded edges in this Embedding.
	 * @return The number of embedded edges in this Embedding.
	 */
	unsigned int size() const;

	/**
	 * Gives a specific edge in this Embedding.
	 *
	 * @param index The index of the asked edge
	 *
	 * @return A const pointer to the asked edge.
	 */
	const Edge* getEdge(int index) const;

	/**
	 * Gives le list of all edges in the embeddings
	 * author: T. Guyet
	 */
	list<const Edge*> getEdges() const;

	/**
	 * Tells if a specific edge in this Embedding has the same node order as the corresponding DFSEdge.
	 *
	 * @param index The index of the edge for which to give sameOrderAsDFSEdge.
	 *
	 * @return A boolean.
	 */
	bool getEdgeOrder(int index) const;

	/**
	 * Return a string representation of this Embedding.
	 */
	string toString() const;

	/**
	 * Tells if this Embedding contains a given edge or not.
	 * Test the equality by using the edges' id.
	 *
	 * @param edge The edge to find in this embedding.
	 *
	 * @return True is this embedding contains an edge with the same id as the given edge.
	 */
	bool containsEdge(const Edge*  edge)const;

	/**
	 * Gives the index of the first edge which contains the given node.
	 * Test the equality by using the nodes' id.
	 *
	 * @param node The node to find in this embedding.
	 *
	 * @return The index of the first edge in this embedding containing the given node,
	 * or -1 if no edges contains it.
	 *
	 */
	int getFirstEdgeIndexFor(const Node*  node) const;

	/**
	 * Tells if this Embedding contains a given node or not.
	 * Test the equality by using the nodes' id.
	 *
	 * @param node The node to find in this embedding.
	 *
	 * @return True is this embedding contains a node with the same id as the given node.
	 */
	bool containsNode(const Node* node) const;

	/**
	 * Gives the index of the given edge in this embedding.
	 * Test the equality by using the edges' id.
	 *
	 * @param edge The edge whose index to find in this embedding.
	 *
	 * @return The index of the given edge in this embedding,
	 * or -1 if it is not present in this Embedding.
	 *
	 */
	int getEdgeIndex(const Edge& edge) const;

	void print(ostream& o) const;

	/**
	 * Gives a set of the nodes of this embedding.
	 * each node appears only once in the resulting set,
	 *  no matter how many edges contains it.
	 *
	 *  @result A pointer to a set of const nodes.
	 */
	set<const Node*,idComparator>* getNodes()const;

private:
	void getEdgesRec(list<const Edge*>&l, int index) const;
};

void testEmbedding();

ostream& operator<<(ostream& o, const Embedding& e);

#endif /* EMBEDDING_H_ */
