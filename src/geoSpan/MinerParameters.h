/*
 * Support.h
 *
 *  Created on: 14 juin 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#ifndef SUPPORT_H_
#define SUPPORT_H_

class Edge;
class DFSCode;
class Embedding;

#include <math.h>
#include <string>
#include <iostream>
#include <fstream>

using namespace std;
/**
 * Parameters used to mine a graph, using an adaptative threshold.
 * This class is an interface for specific implementations.
 * A starting threshold is found for a root pattern (an edge), and then it is incremented
 * as the pattern is specialized.
 */
class MinerParameters {
protected:

	/**
	 * Absolute threshold
	 */
	float maxValue;

	/**
	 * Multiplier used at each incrementation.
	 * It allow to increase, decrease or nullify the effect of the threshold increment.
	 */
	float incrementMultiplier;

	/**
	 * max depth to avoid repetiting
	 */
	unsigned int max_depth;

	/**
	 * output file to provide patterns online
	 */
	ofstream *output;
public:
	/**
	 * Constructor
	 */
	MinerParameters(float max,float inc):maxValue(max),incrementMultiplier(inc), max_depth(0), output(NULL){};
	virtual ~MinerParameters();

	/**
	 * Gives the starting threshold for the given root.
	 * The root must have been sorted before with Edge::sortNodesByFreq().
	 *
	 * @param root A sorted edge
	 *
	 * @return A float representing the starting threshold. Never bigger than maxValue.
	 */
	virtual float findStart(const Edge& root)const=0;

	/**
	 * Gives the current threshold for the given DFSCode;
	 *
	 * @param code A DFSCode whose threshold to return.
	 *
	 * @return A float representing the code's threshold. Never bigger than maxValue.
	 */
	virtual float getThreshold(const DFSCode& code)const=0;


	/**
	 *  Computes the given code's support using it's embeddings.
	 *
	 *  @param code A DFSCode whose support to return.
	 *
	 *  @return A float representing the code's support.
	 */
	virtual float getSupport(const DFSCode& code) const =0;

	/**
	 *  Computes an upper bound for the the given code's support.
	 *
	 *  @param code A DFSCode.
	 *
	 *  @return A float representing an upper bound for the the code's support.
	 */
	virtual float fastSupport(const DFSCode& code) const =0;


	/**
	 * Allows to define what is a valid occurrence of a pattern.
	 *
	 * @param embedding The occurence to test.
	 *
	 * @return true if the occurrence is valid, false otherwise.
	 */
	virtual bool validateEmbedding(Embedding* embedding)const=0;


	float getIncrementMultiplier() const {
		return incrementMultiplier;
	}

	void setIncrementMultiplier(float incrementMultiplier) {
		this->incrementMultiplier = incrementMultiplier;
	}

	float getMaxValue() const {
		return maxValue;
	}

	void setMaxValue(float maxValue) {
		this->maxValue = maxValue;
	}

	unsigned int getMaxDepth() const {
		return max_depth;
	}

	void setMaxDepth(unsigned int md) {
		max_depth = md;
	}

	void setOuput(string filename);

	ostream *getOutput() const {
		return output;
	}

};


/**
 * An implementation of Miner Parameters.
 * The threshold for a pattern is equals to
 * statingThreshold + pow(number of nodes added to the root,2)* incrementMultiplier.
 * The support of a pattern is computed by using the "most restricted edge" method.
 * For an embedding to be valid, its' nodes must have a pair of labels "xCentroid" and "yCentroid"
 * describing a point, and all points must within a given distance from all the other points.
 */
class compactQuadIncNodeParameters:  public MinerParameters {
private:
	/**
	 * Maximum allowed distance between two nodes of an embedding for it to be valid.
	 */
	float maxDistanceBetweenParcels;
public:

	/**
	 * Constructor
	 */
	compactQuadIncNodeParameters(float max,float inc,float maxDistance):MinerParameters(max,inc),maxDistanceBetweenParcels(maxDistance){
		if(inc < 0){
			inc=0;
			cerr<<"The increment multiplier must be positive! It has been set to 0.";
		}
	};

	/**
	 * Construct a compactQuadIncNodeParameters with arguments given by parsing args
	 * @apram args String made as follow : max,inc,maxDistance
	 */
	static compactQuadIncNodeParameters* construct(string args);

	/**
	 * Returns the frequency of root's first edge's labels divided by 5
	 *
	 * @return The frequency of root.nodeA.getLabels()/5.
	 */
	float findStart(const Edge& root)const;


	/**
	 * Returns the code's adaptative threshold.
	 *
	 * @param code A DFSCode whose threshold to return.
	 *
	 * @return A float representing the code's threshold. Never bigger than maxValue.
	 *
	 */
	float getThreshold(const DFSCode& code)const;

	/**
	 * Computes the support of a pattern by using the "most restricted edge" method.
	 * It is the minimum number of unique edges in the database graph that an
	 * edge in the code is mapped to.
	 *
	 * @param code A DFSCode whose support to return.
	 *
	 * @return A float representing the code's support.
	 *
	 */
	float getSupport(const DFSCode& code) const;

	/**
	 *  Return the number of embeddings of this code.
	 *  It is an upper bound for the "most restricted edge" method
	 *
	 *  @param code A DFSCode.
	 *
	 *  @return A float representing the number of embedding for the given code.
	 */
	float fastSupport(const DFSCode& code)const;

	/**
	 * For an embedding to be valid, its' nodes must have a pair of labels "xCentroid" and "yCentroid"
	 * describing a point, and all points must within maxDistanceBetweenParcels from all the other points
	 * in the embedding.
	 *
	 * @param embedding The occurence to test.
	 *
	 * @return true if the occurrence is valid, false otherwise.
	 */
	bool validateEmbedding(Embedding* embedding)const;
};


/**
 * An simple implementation of Miner Parameters that equals to normal mining.
 */
class NoParameters:  public MinerParameters {
private:

public:

	/**
	 * Constructor
	 */
	NoParameters(float max):MinerParameters(max,0){};

	/**
	 * Construct a NoParameters with arguments given by parsing args
	 * @apram args String made as follow : max
	 */
	static NoParameters* construct(string args);


	/**
	 * returns max threshold
	 */
	float findStart(const Edge& root) const {return maxValue;}


	/**
	 * returns max threshold
	 */
	float getThreshold(const DFSCode& code) const {return maxValue;}

	/**
	 * Computes the support of a pattern by using the "most restricted edge" method.
	 * It is the minimum number of unique edges in the database graph that an
	 * edge in the code is mapped to.
	 *
	 * @param code A DFSCode whose support to return.
	 *
	 * @return A float representing the code's support.
	 *
	 */
	float getSupport(const DFSCode& code) const;

	/**
	 *  Return the number of embeddings of this code.
	 *  It is an upper bound for the "most restricted edge" method
	 *
	 *  @param code A DFSCode.
	 *
	 *  @return A float representing the number of embedding for the given code.
	 */
	float fastSupport(const DFSCode& code) const;

	/**
	 * All embedding are valid, no constraint.
	 *
	 * @param embedding The occurence to test.
	 *
	 * @return true, always
	 */
	bool validateEmbedding(Embedding* embedding) const {return true;}
};


#endif /* SUPPORT_H_ */
