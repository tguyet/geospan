/*
 * MinerLauncher.cpp
 *
 *  Created on: 14 juin 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#include "MinerLauncher.h"
#include "Pattern.h"
#include "Miner.h"
#include "MinerParameters.h"
#include "Graph/Node.h"
#include "Graph/Graph.h"
#include <sys/stat.h>

#include <time.h>

#define clearList(x) while(!x.empty())delete x.front(),x.pop_front();

float getDrawingHueValueForNodePF(const Node& node){

#define occValPF(x) (float)x/16.0

	map<string,float> parcelles=map<string,float>();
	parcelles["Autre_culture"] = 							occValPF(1);
	parcelles["Betterave"] = 								occValPF(2);
	parcelles["Surface_boise"] =							occValPF(3);
	parcelles["Colza"] =									occValPF(4);
	parcelles["Crale"] = 									occValPF(5);
	parcelles["Friche"] =									occValPF(6);
	parcelles["Legume_de_plein_champ"] = 					occValPF(7);
	parcelles["Mas"] = 										occValPF(8);
	parcelles["Pois_protagineux"] = 						occValPF(9);
	parcelles["Prairie_seme"] = 							occValPF(10);
	parcelles["Sol_nu"] = 									occValPF(11);
	parcelles["Terre_arable_avec_jeune_culture"] = 			occValPF(12);
	parcelles["Terre_arable_avec_residus"] = 				occValPF(13);
	parcelles["Terre_arable_avec_residus_et_repousse"] =	occValPF(14);
	parcelles["Terre_arable_nue"] = 						occValPF(15);


	return parcelles[node.getStrLabel("parcelOccSol")];
}

void MinerLauncher::launchMiner(string source,string outputDir,MinerParameters* params, bool verbose){

	cout << "reading graph file ...";
	Graph* db=Graph::read(source);
	if (!db) {
		cerr << "Error while opening graph " << source <<endl;
		exit(1);
	}
	cout<<"ok"<<endl;

	Miner* m=new Miner(db, params);

	clock_t start, end;
	double cpu_time_used;

	start = clock();

	cout << "start mining ...";
	list<Pattern*>* results=m->mine(verbose);
	cout << "ok!"<<endl;

	end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;


	mkdir(outputDir.c_str(),0700);

	//results
	int nbPatterns=results->size();
	int nbPaths=0;
	int nbTrees=0;
	int nbCycles=0;

	double percPaths=0;
	double percTrees=0;
	double percCycles=0;

	for(Pattern* p:*results){
		if(p->isCyclic()){
			nbCycles++;
		}else if(p->isTree()){
			nbTrees++;
		}else{
			nbPaths++;
		}
	}

	if(nbPatterns!=0){
		percPaths=100*nbPaths/nbPatterns;
		percTrees=100*nbTrees/nbPatterns;
		percCycles=100*nbCycles/nbPatterns;
	}

	if(verbose){
		cout<<endl<<"__________________"<<endl;
		cout<<"time : "<<cpu_time_used<<endl;
		cout<<"max : "<<params->getMaxValue()<<endl;
		cout<<"inc : "<<params->getIncrementMultiplier()<<endl;
		cout<<"Result : "<<nbPatterns<<" patterns"<<endl;
		cout<<"paths : "<<nbPaths<<" : "<<percPaths<<"%"<<endl;
		cout<<"trees : "<<nbTrees<<" : "<<percTrees<<"%"<<endl;
		cout<<"cycles : "<<nbCycles<<" : "<<percCycles<<"%"<<endl;
	}

	int i=1;
	for(Pattern* p:*results){
		Graph &g = p->getPatternGraph();

		g.draw(outputDir+"pattern"+to_string(i),"png");

		ofstream textFile(outputDir+"pattern"+to_string(i)+".txt");
		textFile<<*p;
		textFile.close();
		i++;
	}

	delete m;
	while(!results->empty())delete results->front(),results->pop_front();
	delete results;
}

