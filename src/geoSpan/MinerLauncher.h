/*
 * MinerLauncher.h
 *
 *  Created on: 14 juin 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#ifndef MINERLAUNCHER_H_
#define MINERLAUNCHER_H_

class Node;
class MinerParameters;

#include <string>
#include <list>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

/**
 * Static library offering static methods to call the miner and write the results.
 */
class MinerLauncher {

public:
	/**
	 * Launch the miner on a graph descripted in the file <it>source</it>, using <it>support</it>.
	 * Results will be placed in in the directory <it>outputDir</it>, which must
	 * contains a sub-directory <it>patterns</it>.
	 * The node will be colored using <it>nodeColoringFunction</it> which return a
	 * float in [0;1] corresponding to the hue to draw the given node with.
	 *
	 * @param source Path to the file containing a description of the graph to mine.
	 * @param outputDir Directory path where to put the results. Must contain a sub-directory <it>patterns</it>
	 * 	to write individual patterns in it.
	 * @param support The parameters to use while mining.
	 * @param verbose If true, will prints steps while mining
	 * @param nodeColoringFunction Function returning a float in [0;1] corresponding to the hue to draw the given node with.
	 */
	static void launchMiner(string source,string outputDir,MinerParameters* support,bool verbose);

};

/**
 * Node coloring method for Plaine Fougère.
 *
 * @param node A node that have a "parcelOccSol" label
 *
 * @return A float in [0;1] corresponding to the "parcelOccSol" label.
 */
float getDrawingHueValueForNodePF(const Node& node);

#endif /* MINERLAUNCHER_H_ */
