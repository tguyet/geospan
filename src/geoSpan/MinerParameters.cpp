/*
 * Support.cpp
 *
 *  Created on: 14 juin 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#include "MinerParameters.h"
#include "DFSCode.h"
#include "DFSEdge.h"
#include "Embedding.h"
#include "Graph/Node.h"
#include "Graph/Edge.h"
#include "Graph/Graphable.h"
#include "Graph/Label.h"
#include "config.h"


void MinerParameters::setOuput(string filename)
{
	output->open(filename.c_str(), std::ofstream::out | std::ofstream::app);
}

MinerParameters::~MinerParameters() {
	if(output) {
		output->close();
		delete output;
	}
}

float compactQuadIncNodeParameters::findStart(const Edge& root)const{

	//use floor for duplicate frequencies -> X.00 , X.01 , ...
	float startValue=floor(root.getNodeA()->getLabels()->getFrequency())/5;

	//for duplicate frequencies -> X.00 , X.01 , ...

	if(startValue<1){
		startValue=1;
	}
	if(startValue>maxValue){
		startValue=maxValue;
	}

	return startValue;
}

float compactQuadIncNodeParameters::getThreshold(const DFSCode& code) const {

	/*
	switch(Experimenteur.type){

	case quadNodeIncrement:
		if(code.get(code.size()-1).isForward()){
			threshold+=(2*(nbNodes()-2)-1)*thresholdInc;
		}
		break;

	case quadEdgeIncrement:
		threshold+=(2*(code.size()-1)-1)*thresholdInc;
		break;

	case simpleEdgeIncrement:
		threshold+=thresholdInc;
		break;

	case simpleNodeIncrement:
		if(code.get(code.size()-1).isForward()){
			threshold+=thresholdInc;
		}
		break;

	}
	 */

	float threshold;
	//quadNodeIncrement
	threshold=findStart(*code.getEmbeddings().front()->getEdge(0))+(code.nbNodes()-2)*(code.nbNodes()-2)*incrementMultiplier;

	if(threshold>maxValue){
		threshold=maxValue;
	}
	return threshold;
}

float compactQuadIncNodeParameters::getSupport(const DFSCode& code) const {

	//the support is at max the number of embedding by def
	float support=fastSupport(code);

	int size=code.getSize();

	float threshold=getThreshold(code);

	//for each edge of the pattern, count how many embedded edge there is
	for(int i=0; i<size && support>=threshold; i++){

		//easiest way to find the number of DIFFERENT embedding of this pattern edge
		//edges are compared by their id.
		set<int> edgeIDSet=set<int>();

		for(Embedding* emb:code.getEmbeddings()){
			int id=emb->getEdge(i)->getID();
			edgeIDSet.insert(id);
		}

		//replace support if it is inferior
		int numberOfDifferentEdges=edgeIDSet.size();

		if(numberOfDifferentEdges<support) {
			support=numberOfDifferentEdges;
		}
	}

	return support;
}

float compactQuadIncNodeParameters::fastSupport(const DFSCode& code) const {
	return code.getEmbeddings().size();
}

compactQuadIncNodeParameters* compactQuadIncNodeParameters::construct(string args) {

	float m,i,maxD;

	int comma1=args.find_first_of(",");
	int comma2=args.find_first_of(",",comma1+1);
	m=atof(args.substr(0,comma1).c_str());
	i=atof(args.substr(comma1+1,comma2-comma1-1).c_str());
	maxD=atof(args.substr(comma2+1).c_str());

	return new compactQuadIncNodeParameters(m,i,maxD);
}

bool compactQuadIncNodeParameters::validateEmbedding(Embedding* embedding) const {
	if(maxDistanceBetweenParcels>300){
		return true;
	}

	bool valid=false;
	float maxDist=0;
	set<const Node*,idComparator>* nodes=embedding->getNodes();
	set<const Node*,idComparator>::const_iterator node1;
	set<const Node*,idComparator>::const_iterator node2;
	for(node1=nodes->begin(); node1!=nodes->end(); node1++) {
		//FIXME aller chercher les coordonnées
		float x1=(*node1)->X();
		float y1=(*node1)->Y();

		for(node2=node1;node2!=nodes->end();node2++) {
			float x2=(*node2)->X();
			float y2=(*node2)->Y();

			float dist=(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);

			if(dist>maxDist) {
				maxDist=dist;
			}
		}
	}
	nodes->clear();
	delete nodes;
	if(maxDist>maxDistanceBetweenParcels*maxDistanceBetweenParcels) {
		valid=false;
	} else {
		valid=true;
	}

	return valid;
}



/**************************************************/


float NoParameters::getSupport(const DFSCode& code) const {


	//the support is at max the number of embedding by def
	float support=fastSupport(code);

	int size=code.getSize();

	float threshold=getThreshold(code);

	//for each edge of the pattern, count how many embedded edge there is
	for(int i=0;i<size && support>=threshold;i++){

		//easiest way to find the number of DIFFERENT embedding of this pattern edge
		//edges are compared by their id.
		set<int> edgeIDSet=set<int>();

		for(Embedding* emb:code.getEmbeddings()){

			int id=emb->getEdge(i)->getID();

			edgeIDSet.insert(id);
		}

		//replace support if it is inferior
		int numberOfDifferentEdges=edgeIDSet.size();

		if(numberOfDifferentEdges<support){
			support=numberOfDifferentEdges;
		}
	}

	return support;
}

NoParameters* NoParameters::construct(string args) {
	float m=atof(args.c_str());

	return new NoParameters(m);

}

float NoParameters::fastSupport(const DFSCode& code) const {
	return code.getEmbeddings().size();
}
