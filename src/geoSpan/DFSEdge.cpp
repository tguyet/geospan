/*
 * DFSEdge.cpp
 *
 *  Created on: 25 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#include "DFSEdge.h"
#include "Graph/Label.h"
#include "Graph/Edge.h"
#include "Graph/Node.h"


DFSEdge::DFSEdge():nodeAIndex(0),nodeBIndex(0){
	nodeALabel=NULL;
	nodeBLabel=NULL;
	edgeLabel=NULL;
}

DFSEdge::DFSEdge(int na, int nb, const Label* la, const Label* lb, const Label* le):nodeAIndex(na),nodeBIndex(nb){
	nodeALabel=new Label(*la);
	nodeBLabel=new Label(*lb);
	edgeLabel=new Label(*le);
}

DFSEdge::DFSEdge(const DFSEdge& other):nodeAIndex(other.nodeAIndex),nodeBIndex(other.nodeBIndex){
	nodeALabel=new Label(*other.nodeALabel);
	nodeBLabel=new Label(*other.nodeBLabel);
	edgeLabel=new Label(*other.edgeLabel);
}


DFSEdge::~DFSEdge() {
	delete nodeALabel;
	delete nodeBLabel;
	delete edgeLabel;
}

Edge* DFSEdge::toEdge()const{
	Node *a,*b;
	a=new Node();
	b=new Node();
	a->setLabels(new Label(*nodeALabel));
	b->setLabels(new Label(*nodeBLabel));

	Edge* e=new Edge(a, b);
	e->sortNodesByFreq();
	e->setLabels(new Label(*edgeLabel));

	return e;
}

//only compare 2 DFSEdge at the same positions in a code
float DFSEdge::compareTo(const DFSEdge& other)const {

	float comp;

	if((!this->isForward()) && other.isForward()){
		return -1;
	}

	if(this->isForward() && (!other.isForward())){
		return 1;
	}

	if(this->isForward() && other.isForward()){//both forward

		// /!\ inverse order here
		comp=other.nodeAIndex-this->nodeAIndex;
		if(comp!=0){
			return comp;
		}

		comp=*this->nodeALabel-*other.nodeALabel;
		if(comp!=0){
			return comp;
		}

		comp=*this->edgeLabel-*other.edgeLabel;
		if(comp!=0){
			return comp;
		}

		comp=*this->nodeBLabel-*other.nodeBLabel;
		if(comp!=0){
			return comp;
		}
	}else{//both backward

		comp=this->nodeBIndex-other.nodeBIndex;
		if(comp!=0){
			return comp;
		}

		comp=*this->edgeLabel-*other.edgeLabel;
		if(comp!=0){
			return comp;
		}
	}

	//equivalent
	return 0;
}


bool DFSEdge::isGoodNextNeighbourgh(const DFSEdge& next)const{
	if(this->isForward()){
		if(next.isForward()){
			return next.nodeAIndex <= this->nodeBIndex && next.nodeBIndex == this->nodeBIndex+1;
		}else{
			return next.nodeAIndex == this->nodeBIndex && next.nodeBIndex < this->nodeAIndex;
		}
	}else{
		if(next.isForward()){
			return next.nodeAIndex <= this->nodeAIndex && next.nodeBIndex == this->nodeAIndex+1;
		}else{
			return next.nodeAIndex == this->nodeAIndex && this->nodeBIndex < next.nodeBIndex;
		}
	}
}



DFSEdge& DFSEdge::operator =(const DFSEdge& o) {
	if (this != &o) {
		delete nodeALabel;
		delete nodeBLabel;
		delete edgeLabel;

		this->nodeAIndex=o.nodeAIndex;
		this->nodeBIndex=o.nodeBIndex;
		this->nodeALabel=new Label(*o.nodeALabel);
		this->nodeBLabel=new Label(*o.nodeBLabel);
		this->edgeLabel=new Label(*o.edgeLabel);

	}

	return *this;
}

bool DFSEdge::operator ==(const Edge& o) const {

	Edge* thisEdge=this->toEdge();
	bool res= ((*thisEdge)==o);

	delete thisEdge->getNodeA();
	delete thisEdge->getNodeB();
	delete thisEdge;
	return res;

}

bool DFSEdge::operator !=(const Edge& o) const {

	Edge* thisEdge=this->toEdge();
	bool res= ((*thisEdge)!=o);

	delete thisEdge->getNodeA();
	delete thisEdge->getNodeB();
	delete thisEdge;
	return res;

}

string DFSEdge::toString() const {

	stringstream s;
	s<<nodeAIndex<<","<<nodeBIndex<<","<<*nodeALabel<<","<<*nodeBLabel<<","<<*edgeLabel;

	return s.str();

}



void DFSEdge::print(ostream& o) const {
	o<<toString();
}

ostream& operator<<(ostream& o, const DFSEdge& e){
	e.print(o);
	return o;
}

void testDFSEdge(){
	/*
	cout<<"-----------------"<<endl<<"Test Graph"<<endl;

	string s1=" {parcelOccSol=Prairie_seme}";
	string s2=" {parcelOccSol=Mais}";
	string s3=" {obst=Road}";
	string s4=" {obst=Hedge}";
	Label* l1=new Label(s1);
	l1->setFrequency(2);
	Label* l2=new Label(s2);
	l2->setFrequency(1);
	Label* l3=new Label(s3);
	l3->setFrequency(2);
	Label* l4=new Label(s4);
	l4->setFrequency(1);

	DFSEdge e1=DFSEdge(0,1,l1,l2,l3);
	DFSEdge e2=DFSEdge(1,2,l1,l2,l3);
	DFSEdge e3=DFSEdge(2,3,l1,l2,l3);
	DFSEdge e4=DFSEdge(0,2,l1,l2,l3);
	DFSEdge e5=DFSEdge(2,0,l1,l2,l3);
	DFSEdge e6=DFSEdge(2,3,l2,l2,l3);
	DFSEdge e7=DFSEdge(2,3,l1,l2,l4);
	DFSEdge e8=DFSEdge(2,0,l1,l2,l4);
	DFSEdge e9=DFSEdge(2,1,l1,l2,l4);

	cout<<"general comp"<<endl;

	cout<<e1<<" < "<<e2<<endl<<"\tassert false : "<<(e1<e2)<<endl;
	cout<<e1<<" < "<<e2<<endl<<"\tassert false : "<<(e1<e3)<<endl;
	cout<<e2<<" < "<<e3<<endl<<"\tassert false : "<<(e2<e3)<<endl;
	cout<<e1<<" < "<<e4<<endl<<"\tassert false : "<<(e1<e4)<<endl;

	cout<<e2<<" < "<<e4<<endl<<"\tassert true : "<<(e2<e4)<<endl;
	cout<<e5<<" < "<<e2<<endl<<"\tassert true : "<<(e5<e2)<<endl;

	cout<<e6<<" < "<<e3<<endl<<"\tassert true : "<<(e6<e3)<<endl;
	cout<<e7<<" < "<<e3<<endl<<"\tassert true : "<<(e7<e3)<<endl;
	cout<<e8<<" < "<<e5<<endl<<"\tassert true : "<<(e8<e5)<<endl;

	cout<<e5<<" < "<<e9<<endl<<"\tassert true : "<<(e5<e9)<<endl;
	cout<<"next neighbourgh comp"<<endl;
	cout<<"assert true : "<<(e1.isGoodNextNeighbourgh(e2))<<endl;
	cout<<"assert true : "<<(e2.isGoodNextNeighbourgh(e3))<<endl;
	cout<<"assert true : "<<(e2.isGoodNextNeighbourgh(e5))<<endl;
	cout<<"assert true : "<<(e8.isGoodNextNeighbourgh(e9))<<endl;
	**/

}

