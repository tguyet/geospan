/*
 * Pattern.cpp
 *
 *  Created on: 29 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *      Author: Thomas Guyet
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#include "Pattern.h"
#include "DFSCode.h"
#include "DFSEdge.h"

#include "Graph/Label.h"
#include "Graph/Graphable.h"
#include "Graph/Edge.h"
#include "Graph/Node.h"
#include "Graph/Graph.h"


Pattern::Pattern(const Graph *minedgraph) {
	patternGraph=new Graph(minedgraph->getNodeDictionary(), minedgraph->getEdgeDictionary());
	support= 0;
	cover= 0;
}

Pattern::Pattern(const DFSCode& code, const Graph *minedgraph) {
	support= code.getSupport();
	cover= code.getCover();
	patternGraph=findPatternGraph(code, minedgraph);
}

Graph* Pattern::findPatternGraph(const DFSCode& code, const Graph *minedgraph) {
	Graph* patternGraph=new Graph(minedgraph->getNodeDictionary(), minedgraph->getEdgeDictionary());
	Graphable::resetIDs();
	bool first = true;
	vector<DFSEdge*>* dfsEdges = code.getCode();
	for (DFSEdge* edge : *dfsEdges) {
		Node* a;
		Node* b;
		//create a new node A or find it
		if (first) {
			//only used for the first edge
			a = new Node();
			a->setLabels(new Label(edge->getNodeALabel()));
			a->setID(edge->getNodeAIndex());
			patternGraph->addNode(a);
			first = false;
		} else {
			//A is always found after the first edge is created
			a = patternGraph->getNode(edge->getNodeAIndex());
		}
		//create a new node B or find it
		if (edge->isForward()) {
			//forward edge
			b = new Node();
			b->setLabels(new Label(edge->getNodeBLabel()));
			b->setID(edge->getNodeBIndex());
			patternGraph->addNode(b);
		} else {
			//backward edge
			b = patternGraph->getNode(edge->getNodeBIndex());
		}
		Edge* e = new Edge(a, b);
		e->setLabels(new Label(edge->getEdgeLabel()));
		//e->mean_attributes = edge->mean_attributes; //Test quanti
		patternGraph->addEdge(e);
	}

	delete dfsEdges;

	return patternGraph;
}

Pattern::~Pattern() {
	delete patternGraph;
}

bool Pattern::isCyclic() const {

	const list<Edge*>& edges=patternGraph->getEdges();
	for(Edge* edge:edges){
		if(edge->getNodeA()->getID()>edge->getNodeB()->getID()){
			return true;
		}
	}

	return false;
}

bool Pattern::isTree() const {
	const list<Edge*>& edges=patternGraph->getEdges();
	list<Edge*>::const_iterator currentEdge=edges.begin();
	Edge* previousEdge=*currentEdge;
	currentEdge++;
	for(;currentEdge!=edges.end();currentEdge++){//not correct if the pattern cycle on previousEdge, but does it matters then?
		if( (*currentEdge)->getNodeA()->getID()<previousEdge->getNodeB()->getID() ){
			return true;
		}
		previousEdge=*currentEdge;
	}

	return false;
}



bool Pattern::operator==(const Pattern& other) const {
	return this->support == other.support && *this->patternGraph == *other.patternGraph;
}


string Pattern::writePatternGraph() const {
	return patternGraph->write();
}

string Pattern::toString() const {
	stringstream s;
	//s<<"-----------------";
	//s<<endl<<"Type : " <<(isCyclic()?"cycle":(isTree()?"tree":"path"));
	s<<"#Support : "<<support<<endl;
	s<<"#Cover : "<<cover<<endl;
	s<<writePatternGraph();

	return s.str();
}

void Pattern::print(ostream& o) const {
	o<<toString();
}



Pattern* Pattern::read(const string& source){

	Pattern* pattern=new Pattern(NULL);

	Graphable::resetIDs();

	string line;
	ifstream reader(source.c_str());

	if (reader.is_open()){

		bool support=false;
		bool cover=false;
		while(reader.good() && !support && !cover){
			getline(reader,line);

			//read support
			if(line.compare(0,10,"Support : ")==0){
				pattern->support=atoi(line.substr(10).c_str());
				support=true;
			}

			//read cover
			if(line.compare(0,8,"Cover : ")==0){
				pattern->cover=atof(line.substr(8).c_str());
				cover=true;
			}
		}

		Graph::read(reader,*pattern->patternGraph);

		reader.close();

	}else{
		return NULL;
	}

	return pattern;
}

ostream& operator <<(ostream& o, const Pattern& p) {
	p.print(o);
	return o;
}

