/*
 * Edge.cpp
 *
 *  Created on: 23 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on : 04 juin 2015
 *	Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on : feb 2022
 * 	Author : Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */


#include "Edge.h"


Edge::Edge(Node* a, Node* b) {
	nodeA = a;
	nodeB = b;
	if(nodeA!=NULL)
		nodeA->add(this);
	if(nodeB!=NULL)
		nodeB->add(this);
	_relationname="";
}


const Node* Edge::getOtherNode(const Node* node) const {
	if (node->getID()==nodeA->getID())
		return nodeB;
	if (node->getID()==nodeB->getID())
		return nodeA;
	return NULL;
}

bool Edge::compareNodes(const Edge& other) const {

	if(this->nodeA==other.nodeA && this->nodeB==other.nodeB)
		return true;

	if(this->nodeB==other.nodeA && this->nodeA==other.nodeB)
		return true;

	return false;
}

void Edge::sortNodesByFreq() {
	if(lessLabelComparator()(nodeB->getLabels(),nodeA->getLabels())){
		reverse();
	}
}

void Edge::reverse() {
	Node* n = nodeA;
	nodeA = nodeB;
	nodeB = n;
}

//TODO inclure le nom de la relation ??
string Edge::toString() const {
	stringstream s;
	s <<getID()<<" : ["<< nodeA->getID()<<" - "<< nodeB->getID()<<"] "<< *(getLabels());
	return s.str();
}


// Version actuelle utilise des comparaisons sur les Labels plutot que sur les fréquence!

float Edge::compareTo(const Edge& other) const {

	if( getLabels()->equalLabels( *other.getLabels() ) ){
		if(getNodeA()->getLabels()->equalLabels( *other.getNodeA()->getLabels() )){// A = A
			return ! getNodeB()->getLabels()->equalLabels( *other.getNodeB()->getLabels());

		}else if( getNodeA()->getLabels()->equalLabels( *other.getNodeB()->getLabels() )){//A = B
			return ! getNodeB()->getLabels()->equalLabels( *other.getNodeA()->getLabels() );

		}else if( getNodeB()->getLabels()->equalLabels( *other.getNodeA()->getLabels() )){//B = A
			return ! getNodeA()->getLabels()->equalLabels( *other.getNodeB()->getLabels());

		}else{//all diff or B = B (anyway, A != A)
			return *(getNodeA()->getLabels())- *(other.getNodeA()->getLabels());
		}
	} else {
		return *getLabels()-*(other.getLabels());
	}
}

bool Edge::compareToByID(const Edge& other) const {

	if( getID()==other.getID() && getLabels() == other.getLabels() ){
		if( getNodeA()->getID()==other.getNodeA()->getID()){// A = A
			return getNodeB()->getID()==other.getNodeB()->getID();

		}else if( getNodeA()->getID()==other.getNodeB()->getID()){//A = B
			return getNodeB()->getID()==other.getNodeA()->getID();

		}else if( getNodeB()->getID()==other.getNodeA()->getID()){//B = A
			return getNodeA()->getID()==other.getNodeB()->getID();

		}
	}
	return false;
}

void Edge::print(ostream& o) const {
	o<<toString();
}

/**
 * strict weak comparator between edges :
 * - compares first : nodeA, then nodeB and finally edge attribute.
 */
bool PreGSpanEdgeFrequencyComparator::operator()(const Edge* e1,const Edge* e2) {

	//Label comparator
	lessLabelComparator comparator;

	//first on node A
	bool nodeAComp=comparator(e1->getNodeA()->getLabels(),e2->getNodeA()->getLabels());

	if( nodeAComp ) { //nodeA of e1 is strictly lower than nodeA of e2
		return 1;
	} else {
		if( e1->getNodeA()->getLabels()->equalLabels( *e2->getNodeA()->getLabels() ) ) {
			//egality between nodes A
			bool nodeBComp=comparator(e1->getNodeB()->getLabels(),e2->getNodeB()->getLabels());
			if( nodeBComp ) { //nodeB of e1 is strictly lower than nodeB of e2
				return 1;
			} else {
				if( e1->getNodeB()->getLabels()->equalLabels( *e2->getNodeB()->getLabels() ) ) {
					bool edgeComp=comparator(e1->getLabels(),e2->getLabels());
					if( edgeComp ) { //edge attribute of A is striclty lower than of edge attribute of B
						return 1;
					} else {
						//strict ordering : equals or greater, the strict weak comparators return false !
						return 0;
					}
				} else {
					return 0;
				}
			}
		} else {
			//egality between nodes A and nodes B
			return 0;
		}
	}
}


/*
*Edge Display
*/
ostream& operator<<(ostream& o, const Edge& e) {
	e.print(o);
	return o;
}
