/*
 * Term.h
 *
 *  Created on: 1 oct. 2012
 *      Author: Thomas Guyet, Inria
 *  Edit on: 04 juin 2015
 *	   Author: Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */
 
#ifndef Term_H_
#define GraphFactoryTerm_H_

#include <string>
#include <list>
#include <map>
#include <iostream>

class Term;

using namespace std;

/*
*	class ValuesSet
*	Object that represent a set of values.
*	All the elements of the set are Terms having the same type (integer, float or string)
*/
class ValuesSet : public list<Term*>{
public:
	typedef enum{ATTRIBUTE_NONE, ATTRIBUTE_INT, ATTRIBUTE_FLOAT, ATTRIBUTE_STR} AttributeType;
protected:
	AttributeType _type;

public:
	ValuesSet();
	ValuesSet(const ValuesSet &);
	virtual ~ValuesSet();

	void push_back(Term* t);

	/**
	 * Suppression de tous les termes
	 */
	void clean();

	ValuesSet::AttributeType type() const {
		return _type;
	}

	void setType(const ValuesSet::AttributeType type) {
		_type=type;
	}

	ostream& tostring(ostream& o) const;
};

/**
 * A term is an object that has a value
 * The value of the term is a string, an integer or a float.
 */
class Term {
protected:
	ValuesSet::AttributeType _type; //< Type de l'attribut
	string str_val;	//< Value in case of ATTRIBUTE_STR
	int int_val;		//< Value in case of ATTRIBUTE_INT
	float flt_val;		//< Value in case of ATTRIBUTE_FLOAT

	int _count; 		// number of term added
	int _id;		// identifier
public:
	Term();
	Term(string val);
	Term(int val);
	Term(float val);
	Term(const Term& t);
	virtual ~Term(){};

	bool operator==(const Term& o) const;

	ValuesSet::AttributeType type() const {
		return _type;
	}

	int inc() {return ++_count;};
	int count() const {return _count;};
	float get_flt() const {return flt_val;};
	int get_int() const {return int_val;};
	string get_str() const {return str_val;};
	unsigned int id() const {return _id;};
	void setId(unsigned int id){_id=id;};

	ostream& tostring(ostream& o) const;
	string tostring() const;
};

/**
 * Printing functions
 */
ostream& operator<<(ostream& o, const ValuesSet& ont);
ostream& operator<<(ostream& o, const Term& ont);

#endif /* Term_H_ */
