/*
 * Graph.h
 *
 *  Created on: 24 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *      Author: Thomas Guyet
 *
 *  Edit on : 04 juin 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#ifndef GRAPH_H_
#define GRAPH_H_

class Node;
class Edge;
class hsv;
class rgb;
class hsv2rgb;

#include <iostream>
#include <sstream>
#include <list>
#include <fstream>
#include <algorithm>
#include <string>

#include "Node.h"
#include "Edge.h"
#include "Label.h"
#include "config.h"
#include "GraphDictionary.h"

using namespace std;

/**
 * Represents a graph whose nodes and edges follows ontologies.
 *
 *
 */
class Graph {
private:
	/**
	 * A pointer to the ontology followed by the nodes.
	 */
	GraphDictionary* nodeDictionary;

	/**
	 * A pointer to the ontology followed by the edges.
	 */
	GraphDictionary* edgeDictionary;

	/**
	 * List of all the nodes in this graph.
	 */
	list<Node*> nodes;

	/**
	 * List of all the edges in this graph.
	 */
	list<Edge*> edges;

	/**
	 * lematize the given string. Used for printing the graph.
	 */
	static string lematize(string s);

public:
	/**
	 * Empty constructor. The ontologies are empty too.
	 */
	Graph();

	/**
	 * Construct an empty graph with the given ontologies.
	 *
	 * @ nodeDictionary A pointer to the ontology followed by the nodes.
	 * @ edgeDictionary A pointer to the ontology followed by the edges.
	 */
	Graph(GraphDictionary* nodeDict,GraphDictionary* edgeDict);

	/**
	 * Destructor. Destroy all the nodes and edges and the two ontologies.
	 */
	virtual ~Graph();

	/**
	 * Delete all the current nodes and add all the given ones.
	 * Uses addNodes to add the nodes.
	 *
	 * @param nodes list of the new nodes.
	 */
	void setNodes(const list<Node*>& nodes);

	/**
	 * Delete all the current edges and add all the given ones.
	 * Uses addEdgees to add the edges.
	 *
	 * @param edges list of the new edges.
	 */
	void setEdges(const list<Edge*>& edges);

	/**
	 * Add a list of nodes to this graph.
	 * Uses addNode to add each node.
	 *
	 * @param nodes list of nodes to add to this graph.
	 */
	void addNodes(const list<Node*>& nodes);

	/**
	 * Add a list of edges to this graph.
	 * Uses addEdge to add each edge.
	 *
	 * @param edges list of edges to add to this graph.
	 */
	void addEdges(const list<Edge*>& edges);

	/**
	 * Add a node to the graph and increment the node's concept occurrences.
	 *
	 * @param node A pointer to the node to add to this graph.
	 *
	 */
	void addNode(Node* node);

	/**
	 * Add an edge to the graph and increment the edge's concept occurrences.
	 *
	 * @param edge A pointer to the edge to add to this graph.
	 *
	 */
	void addEdge(Edge* edge);

	/**
	 * Gives the list of all the nodes in this graph.
	 *
	 * @return A const reference to the list of nodes.
	 */
	const list<Node*>& getNodes() const {return nodes;}

	/**
	 * Gives the list of all the edges in this graph.
	 *
	 * @return A const reference to the list of edges.
	 */
	const list<Edge*>& getEdges() const {return edges;}

	/**
	 * Find a specific node in the graph by it's id.
	 *
	 * @param id The id of the wanted node.
	 *
	 * @return A pointer to the node whose identifiant=id, or nullptr if no node is found.
	 */
	Node* getNode(int id) const;

	/**
	 * Find a specific edge in the graph by it's id.
	 *
	 * @param id The id of the wanted edge.
	 *
	 * @return A pointer to the edge whose identifiant=id, or nullptr if no edge is found.
	 */
	Edge* getEdge(int id) const;


	unsigned int countNodes() const { return nodes.size();};
	unsigned int countEdges() const { return edges.size();};


	/**
	 * Give the ontology followed by the nodes
	 *
	 * @return A reference to the node ontology.
	 */
	GraphDictionary* getNodeDictionary() const {return nodeDictionary;}

	/**
	 * Give the ontology followed by the edges
	 *
	 * @return A reference to the edge ontology.
	 */
	GraphDictionary* getEdgeDictionary() const {return edgeDictionary;}

	/**
	 * Sort the edge list according to the given comparator.
	 * Uses list::sort with the given comparator.
	 *
	 * @param comp The comparator to use. Must define the operator ()(Edge*,Edge*)
	 */
	template<typename _StrictWeakOrdering>
	void sortEdges(_StrictWeakOrdering comp){edges.sort(comp);}

	/**
	 * Sort the nodes list according to the given comparator.
	 * Uses list::sort with the given comparator.
	 *
	 * @param comp The comparator to use. Must define the operator ()(Node*,Node*)
	 */
	template<typename _StrictWeakOrdering>
	void sortNodes(_StrictWeakOrdering comp){nodes.sort(comp);}

	/**
	 *
	 */
	static Graph* read(const string& source);

	static void read(ifstream& source, Graph& graph);

	bool operator==(const Graph& other) const;

	string write() const;

	void draw(string destination, string format) const;

	string toString() const;

	void print(ostream& o) const;

	/**
	 * output format of a graph for TGE tools
	 */
	void printTGE(ostream &p) const;
};

ostream& operator<<(ostream& o, const Graph& g);

#endif /* GRAPH_H_ */
