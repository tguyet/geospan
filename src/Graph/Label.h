/*
 * Label.h
 *
 *  Created on: 22 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *      Author: Thomas Guyet
 *  Edit on: 04 juin 2015
 *	   Author: Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */
 
#ifndef LABEL_H_
#define LABEL_H_


#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <map>
#include <utility>
#include <cctype>
#include <memory>

#include "GraphDictionary.h"

using namespace std;


/**
 * The Label class represents Labels for Graphable components.
 *
 * A Label object is made of a map of couples (attribute, values) and one specific value
 * used to define the Label (numLabel or catLabel).
 * - numLabels and numLabel are used for numerical values
 * - catLabels and catLabel are used for categorical values
 *
 */
class Label {
private:
	/**
	 * Pointer to the ontology this Label uses. Concept has been built from this ontology.
	 */
	GraphDictionary* _dict;

	/**
	 * Map of the numerical Labels in this object. The individuals are from ontology.
	 */
	map<string, Term*> _Labels;

	Term *_mainTerm;

public:
	/**
	 * Empty constructor. No concept, ontology and Labels.
	 */
	Label();

	/**
	 * Construct this object with the given ontology.
	 * The Labels and the concept are found out of the given string.
	 *
	 * @param s A string containing the information needed to build this object.
	 * Must be in the form of "[class=individual,class=individual,...]"
	 * @param ontology A pointer to the ontology to give this object
	 */
	Label(string s, GraphDictionary* onto);

	/**
	 * Copy constructor
	 */
	Label(const Label& Label);

	/**
	 * Destructor. Only reset the shared pointer to the concept, everything else is managed in other place.
	 */
	virtual ~Label(){};

	bool hasLabel(const string &LabelName) const;
	Term *getLabels(const string& LabelName);
	Term *getValue(const string& LabelName) const;

	Term *getMainLabel() const {return _mainTerm;};

	//le dictionnaire est accédé par les graphes !
	//GraphDictionary *getDictionary() const {return _dict;};


	string getStrLabel(const string& attr) const;
	float getFltLabel(const string& attr) const;
	int getIntLabel(const string& attr) const;

	void remove(const string &str) { _Labels.erase(str);};

	/**
	 * Returns the frequency of this object's concept in the mined graph.
	 * It is unique for each concept.
	 *
	 * If there is no Label, the frequency is 0 (this value is meaningful with the comparison operators).
	 *
	 * @return The frequency of this object's concept in the mined graph.
	 */
	float getFrequency() const {
		if(_mainTerm){
			return _mainTerm->count();
		} else {
			return 0;
		}
	};

	/**
	 * Compares the used Labels of this object and other.
	 * It compares the concept of the 2 objects, as they represents all the used Labels.
	 *
	 * @param  other The other Label to compare to this.
	 *
	 * @return True if the concepts are equals.
	 */
	bool equalLabels(const Label& other) const;

	/**
	 * Affectation operator. Copy everything.
	 */
	Label& operator=(const Label& o);

	/**
	 * If the concepts' frequencies are different from 0, compares them, else uses equalLabels.
	 *
	 * @param  o The other Label to compare to this.
	 *
	 * @return True if the two objects are equals, by their frequencies if it set or else by their Labels.
	 */
	//TODO here to take num values in consideration
	bool operator==(const Label& o)const;

	bool fcomp(const Label* o) const;

	/**
	 * If the concepts' frequencies are different from 0, compares them, else uses equalLabels.
	 *
	 * @param  o The other Label to compare to this.
	 *
	 * @return True if the two objects are different, by their frequencies if it set or else by their Labels.
	 */
	bool operator!=(const Label& o) const {return !(*this==o);}

	/**
	 * If the concepts' frequencies are different from 0, subtract them.
	 * Else if equalLabels returns true this operator return 0, otherwise it returns 1.
	 *
	 * @param  o The other Label to substract to this.
	 *
	 * @return the frequencies differences if they are set or 0 or 1 depending on equalsLabels being true or not.
	 */
	//TODO could compare the individuals
	float operator- (const Label& o) const;

	/**
	 * Get only used Labels.
	 */
	string toString() const;

	/**
	 * Get all Labels.
	 */
	string toStringAll() const;

	void print(ostream& o) const;

};

ostream& operator<<(ostream& o, const Label& l);


/**
 * \brief The frequency comparator strictly compare the Label by their frequencies.
 *
 * The strict lower Label is the less frequent Label.
 * For comparing Labels with equal frequencies, we use the addresses of the main terms of the Labels
 * (a Label refers to shared terms).
 * If their are no Labels (mainTerm == NULL), then Labels are equals!
 */
class lessLabelComparator {
public:
	lessLabelComparator(){};
	bool operator()(const Label& l1,const Label& l2) const;
	bool operator()(const Label* l1,const Label* l2) const;
};

#endif /* Label_H_ */
