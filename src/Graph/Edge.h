/*
 * Term.h
 *
 *  Created on: 1 oct. 2012
 *      Author: guyet
 *  Edit on : 04 juin 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#ifndef EDGE_H_
#define EDGE_H_

class Node;

#include <iostream>
#include <sstream>
#include <string>

#include "Label.h"
#include "Node.h"
#include "Graphable.h"

using namespace std;


/**
 * Represents an edge in a graph. An Edge object inherits from Graphable and a label.
 * An edge connects 2 nodes.
 */
class Edge: public Graphable {
private:
	/**
	 * Pointer to the first node connected by this edge.
	 */
	Node* nodeA;

	/**
	 * Pointer to the second node connected by this edge.
	 */
	Node* nodeB;

	/**
	 * Swap the 2 nodes.
	 */
	void reverse();

	/**
	 * Main name of the relation
	 */
	std::string _relationname;

public:
	/**
	 * Empty constructor. Constructs an edge with an empty label and two nullptr for the nodes.
	 */
	//Edge();

	/**
	 * Construct an edge with no label and two nodes.
	 * Add this as an edge for both of the nodes.
	 *
	 * @param a A pointer to the first node connected by this edge.
	 * @param b A pointer to the second node connected by this edge.
	 */
	Edge(Node* a, Node* b);

	/**
	 * Destructor. Do nothing, the nodes are managed by the graph;
	 */
	virtual ~Edge(){};

	/**
	 * Returns the first node connected by this edge.
	 * @return A const reference to the first node connected by this edge.
	 */
	const Node* getNodeA() const{return nodeA;}

	/**
	 * Returns the second node connected by this edge.
	 * @return A const reference to the second node connected by this edge.
	 */
	const Node* getNodeB() const{return nodeB;}

	/**
	 * Returns the node connected by this edge that is not the given node.
	 * This method return nullptr if the given node is not connected by this edge.
	 *
	 * @param node A reference to a node. Preferably one of the two node connected by this edge.
	 *
	 * @return A const pointer to the other node, or nullptr if the given node is not connected by this edge.
	 */
	const Node* getOtherNode(const Node* node) const;


	std::string relation() const {return _relationname;};

	void setRelation(std::string str) {_relationname=str;};

	/**
	 * Compares this edge to a given edge by comparing their nodes.
	 * Uses operator== on the nodes.
	 *
	 * @param  other A const reference to the edge to compare with this one
	 *
	 * @return True if this edge's nodes are equals to other's nodes with the operator==
	 */
	bool compareNodes(const Edge& other)const;


	/**
	 * Compares this edge to a given edge by comparing their labels and their nodes labels.
	 *
	 * @param  other A const reference to the edge to compare with this one
	 *
	 * @return A float that will be < 0 if this < other, == 0 if this == other and > 0 if this > other
	 */
	float compareTo(const Edge& other)const;

	/**
	 * Compares this edge to a given edge by comparing their id and labels and their nodes id.
	 * Used to compare two graphs to each others.
	 *
	 * @param  other A const reference to the edge to compare with this one
	 *
	 * @return True if this edge's and other's id, labels and nodes id are equals to each others
	 */
	bool compareToByID(const Edge& other)const;

	/**
	 * Ensure that node a's frequency is lower than node b's.
	 * If it is not the case, the two node are swapped.
	 */
	void sortNodesByFreq();

	/**
	 * Uses compareTo.
	 */
	bool operator==(const Edge& o) const {return compareTo(o)==0;};

	/**
	 * Uses compareTo.
	 */
	bool operator!=(const Edge& o) const {return compareTo(o)!=0;};

	/**
	 * Uses compareTo.
	 */
	bool operator< (const Edge& o) const {return compareTo(o)<0;};

	/**
	 * Uses compareTo.
	 */
	bool operator> (const Edge& o) const {return compareTo(o)>0;};

	/**
	 * Uses compareTo.
	 */
	bool operator>= (const Edge& o) const{return compareTo(o)>=0;};

	/**
	 * Uses compareTo.
	 */
	bool operator<= (const Edge& o) const {return compareTo(o)<=0;};

	string toString() const;

	void print(ostream& o) const;

	friend ostream& operator<<(ostream& o, const Edge& e);
};



/**
 * Class to compare the pointer to 2 edges.
 * Used to sort the edges of a graph before mining it with the gSpan algorithm.
 * The method sortNodesByFreq must have been applied to the edges before calling this comparator.
 * It compares the labels of the edges' node a, if they are equals it compares the labels of the edges' node b,
 * if they are equals it compares of the edges' labels.
 */
class PreGSpanEdgeFrequencyComparator{
public:
	PreGSpanEdgeFrequencyComparator(){};
	bool operator()(const Edge* e1,const Edge* e2);
};



#endif /* EDGE_H_ */
