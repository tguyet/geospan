/*
 * Graphable.h
 *
 *  Created on: 23 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on : 04 juin 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#ifndef GRAPHABLE_H_
#define GRAPHABLE_H_


#include <string.h>
#include <iostream>
#include <sstream>

#include "Label.h"


using namespace std;

/**
 * The Graphable class represents components used to build a Graph.
 * A Graphable object is made of a unique id and a Label object.
 * Graph component are nodes and edges.
 *
 * @see Graph
 * @see Node
 * @see Edge
 * @see Label
 */
class Graphable {
private:
	/** Used to generate id's for graph components */
	static int idGenerator;

	/** Id of the component. */
	int id;

	/** Map of Labels
	 * @see Label
	 */
	Label* Labels;

public:
	/**
	 * Default constructor. Construct the object with a unique id and an empty Label.
	 */
	Graphable();

	/**
	 * Destructor.
	 */
	virtual ~Graphable();

	/**
	 * Returns the id of the component.
	 * @return The id of the component.
	 */
	int getID() const {return id; }

	/**
	 * Gives the component a new id. May break the unicity of component's id.
	 * @param id The new id for this component.
	 */
	void setID(int id) {(*this).id = id; }

	/**
	 * Returns a string representation of the component.
	 * @return A string representation of the component.
	 */
	virtual string toString() const;

	/**
	 * Prints the string representation of the component in an out stream
	 * @param o the out stream where to print the component
	 */
	virtual void print(ostream& o) const;

	/**
	 * Returns a string representing the Label value asked for.
	 * It is a proxy method for Label::getGraphProto().
	 * @param GraphProtoName the name of the Label which value is asked for.
	 * @see Label::getLabel
	 * @return A string representing the Label value asked for.
	 */
	string getStrLabel(const string& GraphProtoName) const;

	/**
	 * Returns a float representing the Label numerical value asked for.
	 * It is a proxy method for Label::getFloatGraphProto().
	 * @param GraphProtoName the name of the Label which value is asked for.
	 * @see Label::getNumLabel
	 * @return A float representing the Label value asked for.
	 */
	float getFltLabel(const string& GraphProtoName) const;

	int getIntLabel(const string& GraphProtoName) const;

	/**
	 * Returns the Label object for this component.
	 * @return A reference to the Labels.
	 */
	const Label* getLabels() const;
	Label* getLabels();

	/**
	 * Replace the previous Labels with those given.
	 * @param Labels The new Labels for this component.
	 */
	void setLabels(Label* Labels);


	//bool compareLabels(const Graphable& other)const;

	/**
	 * Static method to reset the idGenerator to 0.
	 */
	static void resetIDs(){idGenerator=0;}

	/**
	 * A virtual overloading of the == operator.
	 * Uses both the id and the Label.
	 * @param o The Graphable object to compare to this
	 * @return True if this and o share a same id and Labels.
	 */
	virtual bool operator==(const Graphable& o) const;

	/**
	 * A virtual overloading of the == operator.
	 * Uses both the id and the Label.
	 * @param o The Graphable object to compare to this
	 * @return True if this and o share a same id and Labels.
	 */
	virtual bool operator!=(const Graphable& o) const;

	/**
	 * A virtual overloading of the < operator.
	 * Compares the id of the two components.
	 * @param o The Graphable object to compare to this
	 * @return True if this.id < o.id.
	 */
	virtual bool operator< (const Graphable& o) const;

	/**
	 * A virtual overloading of the > operator.
	 * Compares the id of the two components.
	 * @param o The Graphable object to compare to this
	 * @return True if this.id > o.id.
	 */
	virtual bool operator> (const Graphable& o) const;


};


/**
 * Class to compare two Graphable objects pointer by their ids.
 */
class idComparator {
public:
	idComparator(){};
	/**
	 * Perform weak strict comparison of two Graphable objects pointer by their ids.
	 * @param g1 A const pointer to the first Graphable object.
	 * @param g2 A const pointer to the second Graphable object.
	 * @return True if g1->getID() < g2->getID()
	 */
	bool operator()(const Graphable* g1,const Graphable* g2);
};

/**
 * Class to compare two Graphable objects pointer by their Labels.
 */
class GraphProtoComparator {
public:
	GraphProtoComparator(){};
	/**
	 * Perform weak strict comparison of two Graphable objects pointer by their Labels.
	 * @param g1 A const pointer to the first Graphable object.
	 * @param g2 A const pointer to the second Graphable object.
	 * @return True if g1->getLabels() < g2->getLabels()
	 */
	bool operator()(const Graphable* g1,const Graphable* g2);
};

ostream& operator<<(ostream& o, const Graphable& g);

#endif /* GRAPHABLE_H_ */
