/*
 * GraphDictionary.cpp
 *
 *  Created on: 1 oct. 2012
 *      Author: guyet
 *  Edit on: 04 juin 2015
 *	   Author: Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#include "config.h"

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "GraphDictionary.h"
 
using namespace std;

//Constructors/Destructor
GraphDictionary::GraphDictionary() {}

GraphDictionary::GraphDictionary(const GraphDictionary &dic):map<string, ValuesSet>(dic), _attributeUsed(dic._attributeUsed) {

}

void GraphDictionary::clean()
{
	GraphDictionary::iterator it=begin();
	while( it!=end() ) {
		(*it).second.clean();
		it++;
	}
}

//Graph Operations
Term * GraphDictionary::addValue(string str, string val) {
	GraphDictionary::iterator it = addAttribute(str, ValuesSet::ATTRIBUTE_STR);
	if( (*it).second.type()!=ValuesSet::ATTRIBUTE_STR ) {
		cerr << "Error GraphDictionary::addValue : wrong value type, expected STR for attribute " << str <<endl;
		return 0;
	}

	Term *t=new Term(val);
	ValuesSet::iterator itt=(*it).second.begin();
	while( itt!=(*it).second.end() ) {
		if( (*itt)->operator ==(*t) ) {
			break;
		}
		itt++;
	}

	if( itt!=(*it).second.end() ) {
		//le terme existe déjà, on incrémente simplement son nombre
		(*itt)->inc();
		delete t;
		return (*itt);
	} else {
		//le terme n'existe pas, on l'ajoute
		(*it).second.push_back( t );
		return t;
	}
}

Term * GraphDictionary::addValue(string str, int val) {
	GraphDictionary::iterator it = addAttribute(str, ValuesSet::ATTRIBUTE_INT);
	if( (*it).second.type()!=ValuesSet::ATTRIBUTE_INT ) {
		cerr << "Error GraphDictionary::addValue : wrong value type, expected INT for attribute " << str <<endl;
		return 0;
	}

	Term *t=new Term(val);
	ValuesSet::iterator itt=(*it).second.begin();
	while( itt!=(*it).second.end() ) {
		if( (*itt)->operator ==(*t) ) {
			break;
		}
		itt++;
	}

	if( itt!=(*it).second.end() ) {
		//le terme existe déjà, on incrémente simplement son nombre
		(*itt)->inc();
		delete t;
		return (*itt);
	} else {
		//le terme n'existe pas, on l'ajoute
		(*it).second.push_back( t );
		return t;
	}
}

Term * GraphDictionary::addValue(string str, float val) {
	GraphDictionary::iterator it = addAttribute(str, ValuesSet::ATTRIBUTE_FLOAT);
	if( (*it).second.type()!=ValuesSet::ATTRIBUTE_FLOAT ) {
		cerr << "Error GraphDictionary::addValue : wrong value type, expected FLT for attribute " << str <<endl;
		return 0;
	}

	Term *t=new Term(val);
	ValuesSet::iterator itt=(*it).second.begin();
	while( itt!=(*it).second.end() ) {
		if( (*itt)->operator ==(*t) ) {
			break;
		}
		itt++;
	}

	if( itt!=(*it).second.end() ) {
		//le terme existe déjà, on incrémente simplement son nombre
		(*itt)->inc();
		delete t;
		return (*itt);
	} else {
		//le terme n'existe pas, on l'ajoute
		(*it).second.push_back( t );
		return t;
	}
}


bool GraphDictionary::hasAttribute(string attr) const {
	GraphDictionary::const_iterator it=find(attr);
	if( it==end()){
		return false;
	}
	return true;
}


GraphDictionary::iterator GraphDictionary::addAttribute(string str, ValuesSet::AttributeType t) {
	GraphDictionary::iterator it=find(str);
	if( it==end() ) {
		ValuesSet set=ValuesSet();
		set.setType(t);
		insert( pair<string,ValuesSet>(str, set) );
		it=find(str);
	}
	return it;
}


/**
 * Convertion sans notation exponentielle !!
 * une chaîne vide n'est pas un nombre
 */
float convert(const char *str, bool *ok=NULL, bool *isint=NULL) {
	float f=0;
	int i=0;
	char c=str[0];
	bool isneg=false;
	if(isint) *isint=true;
	if(ok) *ok=true;
	int dec=0;
	while(c!=0) {
		if( c=='-') {
			if(i!=0) {
				if(ok) *ok=false;
				return 0.0;
			}
			isneg=true;
		/*} else if( c=='e') {
			if(isint) isint=false;
		 */
		} else if( c==',' || c=='.') {
			if(isint) *isint=false;
			if(dec>0) { //second symbole .
				if(ok) *ok=false;
				return 0.0;
			}
			dec=1;
		} else if( !isdigit(c) ) {
			if(ok) *ok=false;
			return 0.0;
		} else {
			if(!dec) {
				f=10*f+( (int)(c)-48 );
			} else {
				f=f+( (int)(c)-48 )*pow(10,-dec);
				dec++;
			}
		}

		i++;
		c=str[i];
	}
	if(i==0) { //empty string
		if(ok) *ok=false;
	}
	if(isneg) {
		return -f;
	} else {
		return f;
	}
}


/**
 * Fonction pour la construction d'un dictionnaire des attributs/valeurs à partir d'un fichier au format de graphe.
 * L'attribut qui sert d'attribut principal est le premier attribut de la liste du premier noeud (ou du premier arc) !
 * Les types de attributs sont définis en utilisant également le premier noeud et le premier arc : en regardant sa valeur,
 * on prend comme type, le type le plus spécifique.
 */
pair<GraphDictionary *, GraphDictionary *>GraphDictionary::readFromGraph(ifstream& source) {
	GraphDictionary* nodeGraphProtos=new GraphDictionary();
	GraphDictionary* edgeGraphProtos= new GraphDictionary();

#if !defined(NDEBUG) and DEBUG_LEVEL>=2
	cout << "GraphDictionary::readFromGraph Enter" << endl;
#endif

	string line;
	while(source.good()){
		getline(source,line);
		if(line.size()==0){
			continue;
		}

		//read nodeGraphProtos
		if(line.compare(0,1,"v")==0){
			int nextSpace=line.find_first_of("[", 2);

			if(nextSpace==-1) {
				cerr << "GraphDictionary::readFromGraph reading error, expected '['" << endl;
				return make_pair(nodeGraphProtos, edgeGraphProtos);
			}

			string s=line.substr(nextSpace);

			//get rid of all spaces
			s.erase(remove_if(s.begin(), s.end(), (int(*)(int))isspace), s.end());

			//trim enclosing { }
			s=s.substr(1, s.size()-2);

			//find each pair (key=value)
			size_t start=0;
			size_t end=s.find(',');

			bool usedDefined=false;

			while(1){
				string pv;
				if(end!=string::npos) {
					pv=s.substr(start,end-start);
				} else {
					pv=s.substr(start);
				}
				int eq=pv.find('=');

				string sclass=pv.substr(0,eq);
				string sval=pv.substr(eq+1);

				float f;
				//cout << "attribute " << sclass << ", value " << sval <<", type ";
				bool ok, isint;
				f=convert(sval.c_str(), &ok, &isint);
				if(ok) {
					if( !isint ) {
						//On a un nombre à virgule
						nodeGraphProtos->addAttribute(sclass, ValuesSet::ATTRIBUTE_FLOAT);
						nodeGraphProtos->addValue(sclass, f);
						//cout << "FLOAT " << f <<endl;
					} else {
						//On a un entier
						nodeGraphProtos->addAttribute(sclass, ValuesSet::ATTRIBUTE_INT);
						nodeGraphProtos->addValue(sclass, (int)(f));
						//cout << "INT " << (int)f <<endl;
					}
				} else {
					//On a une chaîne de caractères
					nodeGraphProtos->addAttribute(sclass, ValuesSet::ATTRIBUTE_STR);
					nodeGraphProtos->addValue(sclass, sval);
					//cout << "STR"<<endl;
				}

				if(!usedDefined) {
					nodeGraphProtos->setUsed(sclass);
					usedDefined=true;
				}

				if(end==string::npos) {
					break;
				}

				start=end+1;
				end=s.find(',',start);
			}

		} else  if(line.compare(0,1,"u")==0){ //read edgeGraphProtos
			int nextSpace=line.find_first_of("[", 2);
			//int lastSpace=line.find_first_of("]", nextSpace+1);

			if(nextSpace==-1) {
				cerr << "GraphDictionary::readFromGraph reading error, expected '['" << endl;
				return make_pair(nodeGraphProtos, edgeGraphProtos);
			}

			string s=line.substr(nextSpace);

			//get rid of all spaces
			s.erase(remove_if(s.begin(), s.end(), (int(*)(int))isspace), s.end());
			//trim enclosing { }
			s=s.substr(1, s.size()-2);

			//find each pair (key=value)
			size_t start=0;
			size_t end=s.find(',');

			bool usedDefined=false;

			while(1){
				string pv;
				if(end!=string::npos) {
					pv=s.substr(start,end-start);
				} else {
					pv=s.substr(start);
				}
				int eq=pv.find('=');

				string sclass=pv.substr(0,eq);
				string sval=pv.substr(eq+1);

				float f;
				if( sscanf(sval.c_str(), "%f", &f)==1 ) {
					int i;
					if( sscanf(sval.c_str(), "%d", &i)==1 ) {
						//On a un entier
						edgeGraphProtos->addAttribute(sclass, ValuesSet::ATTRIBUTE_INT);
						edgeGraphProtos->addValue(sclass, i);
					} else {
						//On a un nombre à virgule
						edgeGraphProtos->addAttribute(sclass, ValuesSet::ATTRIBUTE_FLOAT);
						edgeGraphProtos->addValue(sclass, f);
					}
				} else {
					//On a une chaîne de caractères
					edgeGraphProtos->addAttribute(sclass, ValuesSet::ATTRIBUTE_STR);
					edgeGraphProtos->addValue(sclass, sval);
				}

				if(!usedDefined) {
					edgeGraphProtos->setUsed(sclass);
					usedDefined=true;
				}

				if(end==string::npos) {
					break;
				}

				start=end+1;
				end=s.find(',',start);
			}
		}
	}


#if !defined(NDEBUG) and DEBUG_LEVEL>=2
	cout << "GraphDictionary::readFromGraph Exit" << endl;
#endif

	return pair<GraphDictionary*, GraphDictionary* >(nodeGraphProtos,edgeGraphProtos);
}


/*
*GraphDictionary Display
*/
ostream& operator<<(ostream& o, const GraphDictionary& ont)
{
	GraphDictionary::const_iterator it=ont.begin();
	while(it!=ont.end()) {
		o << "* " << (*it).first << ":" << endl;
		(*it).second.tostring(o);
		o << endl;
		it++;
	}
	return o;
}
