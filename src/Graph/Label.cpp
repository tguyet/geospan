/*
 * Label.cpp
 *
 *  Created on: 22 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *      Author: Thomas Guyet
 *  Edit on: 04 juin 2015
 *	   Author: Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#include <limits>
#include <string>

#include "Label.h"
#include "config.h"


using namespace std;

/*
*Label Constructor/Destructor
*/
Label::Label() {
	_dict=0;
	_mainTerm=0;
}

Label::Label(const Label& other){
	_dict=other._dict;
	_Labels=map<string,Term*>(other._Labels);
	_mainTerm=other._mainTerm;
}


Label::Label(string s, GraphDictionary* dict) {

#if !defined(NDEBUG) and DEBUG_LEVEL>=3
	cout << "create Label from string : " << s << endl;
#endif

	_mainTerm=0;
	_dict=dict;
	if( dict==0 ) {
		cerr << "Error Label::Label: no dictonary when created a Label"<< endl;
		exit(0);
	}

	//get rid of all spaces
	s.erase(remove_if(s.begin(), s.end(), (int(*)(int))isspace), s.end());
	//trim enclosing { }
	s=s.substr(1, s.size()-2);

	//find each pair (key=value)
	size_t start=0;
	size_t end=s.find(',');

	while(1) {
		//Pour chaque pair d'attribut valeur dans la chaîne de caractères s
		string pv;
		if( end!=string::npos ) {
			//Cas normal
			pv=s.substr(start, end-start);
		} else {
			//Cas du dernier élément de la liste
			pv=s.substr(start);
		}

		int eq=pv.find('=');

		//sclass donne le nom de l'attribut
		string sclass=pv.substr(0,eq);


		if( sclass!="" ) {
			#if !defined(NDEBUG) and DEBUG_LEVEL>=4
				cout << "\treading attribute name : " << sclass;
				if( _dict->used()==sclass ) {
					cout << " (*)";
				}
				cout  << endl;
			#endif

			ValuesSet vs=(*_dict)[sclass];

			Term *t=NULL;
			int i;
			float f;
			switch( vs.type() ) {
			case ValuesSet::ATTRIBUTE_STR:
				t=_dict->addValue(sclass, pv.substr(eq+1) );
				_Labels[sclass]=t;
				break;
			case ValuesSet::ATTRIBUTE_INT:
				i=atoi(pv.substr(eq+1).c_str());
				t=_dict->addValue(sclass, i );
				_Labels[sclass]=t;
				break;
			case ValuesSet::ATTRIBUTE_FLOAT:
				f=atof(pv.substr(eq+1).c_str());
				t=_dict->addValue(sclass, f );
				_Labels[sclass]=t;
				break;
			default:
				cerr << "Error Label::Label(string) : no attribute '"<<sclass <<"' in the dictionary" <<endl;
				exit(1);
			}

			if( t==NULL ) {
				cerr << "Error Label::Label(string) : null Term"<<endl;
			}

	#if !defined(NDEBUG) and DEBUG_LEVEL>=4
			cout << "\treading attribute value : " << *t;
			cout << endl;
	#endif
			if( _dict->used()==sclass ) {
				_mainTerm=t;
			}
		}

		if(end==string::npos) {
			//Fin de la boucle : on vient de traiter le dernier élément de la liste!
			break;
		}

		start=end+1;
		end=s.find(',',start);
	}

}


/*
*Label Get/Set
*/


Term *Label::getLabels(const string& LabelName) {
	return _Labels[LabelName];
}

Term *Label::getValue(const string& LabelName) const {
	map<string, Term*>::const_iterator it=_Labels.find(LabelName);
	if( it==_Labels.end() ) {
		return NULL;
	}
	return (*it).second;
}

string Label::getStrLabel(const string& attr) const
{
	Term *t=getValue(attr);
	if( !t ) {
		cerr << "ERROR Label::getStrLabel : no attribute " << attr <<endl;
		return string("");
	}
	if( t->type()!=ValuesSet::ATTRIBUTE_STR) {
		cerr << "ERROR Label::getStrLabel : wrong type for attribute " << attr <<endl;
		return string("");
	}
	return t->get_str();
}

float Label::getFltLabel(const string& attr) const
{
	Term *t=getValue(attr);
	if( !t ) {
		cerr << "ERROR Label::getFltLabel : no attribute " << attr <<endl;
		return 0.0;
	}
	if( t->type()!=ValuesSet::ATTRIBUTE_FLOAT) {
		cerr << "ERROR Label::getFltLabel : wrong type for attribute " << attr <<endl;
		return 0.0;
	}
	return t->get_flt();
}

int Label::getIntLabel(const string& attr) const
{
	Term *t=getValue(attr);
	if( !t ) {
		cerr << "ERROR Label::getIntLabel : no attribute " << attr <<endl;
		return 0;
	}
	if( t->type()!=ValuesSet::ATTRIBUTE_INT) {
		cerr << "ERROR Label::getIntLabel : wrong type for attribute " << attr <<endl;
		return 0;
	}
	return t->get_int();
}


/*
*Label Comparators
*/
bool Label::hasLabel(const string &LabelName) const {
	map<string, Term*>::const_iterator it=_Labels.find(LabelName);
	return it!=_Labels.end();
}


bool Label::equalLabels(const Label& other) const {
	//On fait bien un test d'égalité de pointeur !
	return (_mainTerm==other._mainTerm);
}




bool Label::operator==(const Label& o) const {
	if( getFrequency()*o.getFrequency()!=0){
		return getFrequency()==o.getFrequency();
	}else{
		return equalLabels(o);
	}
}

bool Label::fcomp(const Label* o) const {
	if( getFrequency()*o->getFrequency()!=0){
		return getFrequency()==o->getFrequency();
	}else{
		return equalLabels(*o);
	}
}

/*
*Label operators
*/
Label& Label::operator=(const Label& o){
	if (this != &o) {
		_Labels.clear();
		_Labels=map<string,Term*>(o._Labels);
		_mainTerm=o._mainTerm;
		_dict=o._dict;
	}
	return *this;
}

float Label::operator -(const Label& o) const {
	if(getFrequency()*o.getFrequency()!=0){
		return getFrequency()-o.getFrequency();
	}else{
		return equalLabels(o)?0:1;
	}
}

bool lessLabelComparator::operator()(const Label& l1, const Label& l2) const
{
	if( l1.getFrequency()==l2.getFrequency() ) {
		return l1.getMainLabel() < l2.getMainLabel();
	} else {
		return l1.getFrequency()<l2.getFrequency();
	}
}

bool lessLabelComparator::operator() (const Label* l1, const Label* l2) const
{
	if( l1->getFrequency()==l2->getFrequency() ) {
		return l1->getMainLabel()<l2->getMainLabel();
	} else {
		return l1->getFrequency()<l2->getFrequency();
	}
}

/*
*GrpahProto Display
*/



/*
 * Affiche uniquement l'attribut utilisé !
 */
string Label::toString() const {
	stringstream s;
	s<<"[";
	for(pair<string,Term *> l:_Labels){
		if( l.first==_dict->used() ){
			s<<l.first<<"="<< *(l.second);
			break;
		}
	}
	s<<"]";

	return s.str();
}

/**
 * \brief print all the attribute, the reference attribute (used by dictionnary) is put at the begining
 */
string Label::toStringAll() const {
	stringstream s;
	s<<"[";

	for(pair<string,Term *> l:_Labels){
		if( l.first==_dict->used() ){
			s<<l.first<<"="<< *(l.second);
			if( _Labels.size()>1 ) s<<",";
			break;
		}
	}
	map<string, Term*>::const_iterator it=_Labels.begin();
	while(it!=_Labels.end()) {
		if( (*it).first==_dict->used() ) {
			it++;
			continue;
		}
		s<< (*it).first<<"="<< *((*it).second);
		it++;
		if( (*it).first==_dict->used() ) it++;
		if( it!=_Labels.end() ) s<<",";
	}
	s<<"]";

	return s.str();
}


void Label::print(ostream& o) const {
	o<<toString();
}

ostream& operator<<(ostream& o, const Label& l){
	l.print(o);
	return o;
}


