/*
 * Node.h
 *
 *  Created on: 23 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: 04 juin 2015
 *	   Author: Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#ifndef NODE_H_
#define NODE_H_

#include "gdal/ogrsf_frmts.h"
#include "gdal/gdal.h"

#include <list>
#include <algorithm>
#include <sstream>
#include "Graphable.h"
#include "config.h"

class Edge;

using namespace std;


/**
 * Represents a node in a graph. 
 * A Node object inherits from Graphable. It holds an id and a Label.
 * A node can have multiples inedges and outedges.
 */
class Node: public Graphable {

private:
	/**
	 * List of pointers to the edges leading to this node
	 */
	list<Edge*> edges;


	//TODO ajouter les géométrie en wkb point
	// Peut être supprimmer les floats  x et y? 
	float _x, _y; //coordonnées du noeuds;

	/**
	*	Geometries stored in wkbpoint
	*/
	unsigned char* wkbPoint;

public:
	/**
	 * Empty constructor. Constructs a node with an empty Label and no edges leading to it.
	 */
	Node();
	Node(float x, float y, const Label &l);
	Node(OGRGeometry *poGeometry, const Label &l);

	/**
	 * Destructor. Doesn't destroy the edges, this is the responsibility of the graph.
	 */
	virtual ~Node();

	void setX(float x) {_x=x;};
	void setY(float y) {_y=y;};
	float X() const {return _x;};
	float Y() const {return _y;};

	/**
	 * Adds an edge to the list of edges leading to this node.
	 *
	 * @param e A pointer to the edge to add to the list of edges leading to this node.
	 */
	void add(Edge* e);

	/**
	 * Removes an edge to the list of edges leading to this node. The edge is not destroyed.
	 *
	 * @param e A pointer to the edge to remove to the list of edges leading to this node.
	 */
	void remove(Edge* e);

	/**
	 * Returns the degree of this node. The degree of a node is the number of different edges leading to it.
	 *
	 * @return The degree of this node.
	 */
	int getDegree() const;

	/**
	 * Gives the list of edges leading to this node.
	 *
	 * @return A const reference to the list of edges leading to this node.
	 */
	const list<Edge*>& getEdges()const{return edges;}

    string toString() const;

    void print(ostream& o) const;
    double distance(const Node &) const;

	unsigned char* getWkbPoint() const {
		if(wkbPoint == nullptr){
			std::cerr << "Get wkbPoint failed.";
			exit(1);
		}
		else{
			return wkbPoint;
		}
	}
};


ostream& operator<<(ostream& o, const Node& n);
#endif /* NODE_H_ */
