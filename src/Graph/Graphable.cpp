/*
 * Graphable.cpp
 *
 *  Created on: 23 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Change on : 04 juin 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#include "Graphable.h"
#include "Label.h"

int Graphable::idGenerator = 0;

/*
*Graphable Constructor/Destructor
*/
Graphable::Graphable() {
	id = Graphable::idGenerator++;
	Labels = new Label();
}

Graphable::~Graphable() {
	delete Labels;
}

/*
*Graphable comparator
*/
string Graphable::getStrLabel(const string& LabelName) const {
	return Labels->getStrLabel(LabelName);
}

float Graphable::getFltLabel(const string& LabelName) const {
	return Labels->getFltLabel(LabelName);
}

int Graphable::getIntLabel(const string& LabelName) const {
	return Labels->getIntLabel(LabelName);
}


void Graphable::setLabels(Label* ls) {
	delete Labels;
	Labels = ls;
}


const Label* Graphable::getLabels() const {
	return Labels;
}
Label* Graphable::getLabels() {
	return Labels;
}


/*
*Graphable comparator
*/
bool idComparator::operator ()(const Graphable* g1, const Graphable* g2) {
	return g1->getID() < g2->getID();
}

bool GraphProtoComparator::operator ()(const Graphable* g1, const Graphable* g2) {
	return g1->getLabels()->getFrequency() < g2->getLabels()->getFrequency();
}


bool Graphable::operator ==(const Graphable& o) const {
	return id == o.id && this->Labels == o.Labels; //TG: pourquoi comparer les Labels si les id sont identiques ??
}

bool Graphable::operator !=(const Graphable& o) const {
	return id != o.id || this->Labels != o.Labels;
}

bool Graphable::operator <(const Graphable& o) const {
	return id<o.id;
}

bool Graphable::operator >(const Graphable& o) const {
	return id>o.id;
}

/*
*Graphable Display
*/
string Graphable::toString() const {
	stringstream out;
	out << id;
	return out.str();
}

void Graphable::print(ostream& o) const {
	o << toString();
}

ostream& operator<<(ostream& o, const Graphable& g) {
	g.print(o);
	return o;
}
