/*
 * Node.cpp
 *
 *  Created on: 23 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr) 
 *  EDIT on: 04 juin 2015
 *	   Author: Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#include "Node.h"
#include "Edge.h"
#include "Label.h"

/*
*Node Constructor/Destructor
*/

Node::Node():_x(0.0), _y(0.0) {
	edges=list<Edge*>();
}

Node::Node(float x, float y, const Label &l):_x(x), _y(y){
	setLabels( new Label(l)) ;
}

Node::Node(OGRGeometry *poGeometry, const Label &l){
	if(poGeometry->WkbSize() > 0){
		wkbPoint = new unsigned char [poGeometry->WkbSize()];
		if(poGeometry->exportToWkb(wkbNDR, wkbPoint, wkbVariantOldOgc) != OGRERR_NONE){
			std::cerr << "Exporting to Wkb polygon failed." << std::endl;
			exit(1);
		}
		//TODO:
	}
	setLabels( new Label(l)) ;
} 


/*
 * The edges are managed by Graph. They are deleted there.
 */
Node::~Node() {
	edges.clear();
}

void Node::remove(Edge* e) {
	edges.remove(e);
}

/*
*Node Accessor 
*/
int Node::getDegree() const{
	//since edges that loop on a node add 2 to the degree
	// of the node, the degree is not simply the size of the edge
	// list
	int degree = 0;

	for(const Edge* edge:edges){
		if (edge->getNodeA()->getID()==getID()){
			degree++;
		}
		if (edge->getNodeB()->getID()==getID()){
			degree++;
		}
	}

	return degree;
}

string Node::toString() const {
	stringstream s;
	s << getID() << " " << getLabels()->toStringAll();
	return s.str();
}

void Node::print(ostream& o) const {
	o<<toString();
}

double Node::distance(const Node &n) const
{
	return (n.X()-X())*(n.X()-X()) + (n.Y()-Y())*(n.Y()-Y());
}

/*
*Node Operation
*/
void Node::add(Edge* e) {
	edges.push_back(e);
}

/*
*Node Display
*/
ostream& operator<<(ostream& o, const Node& n){
	n.print(o);
	return o;
}



