/*
 * Term.cpp
 *
 *  Created on: 1 oct. 2012
 *      Author: guyet
 *  Edit on: 04 juin 2015
 *	   Author: Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#include "Term.h"

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "config.h"


using namespace std;

/*
*ValuesSet
*Constructor/Destructor
*/
ValuesSet::ValuesSet():_type(ValuesSet::ATTRIBUTE_NONE) {}

ValuesSet::ValuesSet(const ValuesSet &vs):_type(vs._type)
{
	for(Term* t:vs) {
		push_back(new Term(*t));
	}
}

ValuesSet::~ValuesSet() {
	clean();
}

void ValuesSet::clean() {
	iterator it=begin();
	while(it!=end()) {
		if(*it) delete(*it);
		*it=NULL;
		it++;
	}
}

/*
*ValuesSet Operation
*/

void ValuesSet::push_back(Term* t) {
	t->setId(size());
	list<Term*>::push_back(t);
}

/*
*ValuesSet Display
*/
ostream& operator<<(ostream& o, const ValuesSet& ont)
{
	ValuesSet::const_iterator it=ont.begin();
	for(auto it : ont){
		o << "\t- " << (*it) << endl;
	}
	return o;
}

ostream& ValuesSet::tostring(ostream& o) const
{
	ValuesSet::const_iterator it=begin();
	while(it!=end()) {
		o << "\t- ";
		(*it)->tostring(o);
		o << endl;
		it++;
	}
	return o;
}

/*
*Term
*Constructor/Destructor
*/
Term::Term():_type(ValuesSet::ATTRIBUTE_NONE), str_val(), int_val(0), flt_val(0.0), _count(0) {}

Term::Term(string val):_type(ValuesSet::ATTRIBUTE_STR), str_val(val), int_val(0), flt_val(0.0), _count(0) {}

Term::Term(int val):_type(ValuesSet::ATTRIBUTE_INT), str_val(), int_val(val), flt_val(0.0), _count(0) {}

Term::Term(float val):_type(ValuesSet::ATTRIBUTE_FLOAT), str_val(), int_val(0), flt_val(val), _count(0) {}

Term::Term(const Term& t):_type(t._type), str_val(t.str_val), int_val(t.int_val), flt_val(t.flt_val), _count(0) {}


/*
*Term Comparator
*/
bool Term::operator==(const Term& o) const
		{
	if( _type!= o.type()) {
		return false;
	}

	switch(_type) {
	case ValuesSet::ATTRIBUTE_FLOAT:
		return flt_val==o.flt_val;
	case ValuesSet::ATTRIBUTE_INT:
		return int_val==o.int_val;
	case ValuesSet::ATTRIBUTE_STR:
		return str_val==o.str_val;
	case ValuesSet::ATTRIBUTE_NONE:
		return true;
	}
	return false;
		}


/*
*Term affichage
*/
ostream& operator<<(ostream& o, const Term& ont)
{
	switch(ont.type()) {
	case ValuesSet::ATTRIBUTE_FLOAT:
		o << ont.get_flt();
		break;
	case ValuesSet::ATTRIBUTE_INT:
		o << ont.get_int();
		break;
	case ValuesSet::ATTRIBUTE_STR:
		o << ont.get_str();
		break;
	case ValuesSet::ATTRIBUTE_NONE:
		o<< "??";
		break;
	}
	return o;
}

ostream& Term::tostring(ostream& o) const
{
	switch(type()) {
	case ValuesSet::ATTRIBUTE_FLOAT:
		o << "(f) " << get_flt() << " (" << count() << ")" ;
		break;
	case ValuesSet::ATTRIBUTE_INT:
		o << "(d) " << get_int() << " (" << count() << ")" ;
		break;
	case ValuesSet::ATTRIBUTE_STR:
		o << "(s) " << get_str() << " (" << count() << ")" ;
		break;
	case ValuesSet::ATTRIBUTE_NONE:
		o << "(null)";
		break;
	}
	return o;
}

string Term::tostring() const {
	stringstream ss;
	switch(type()) {
	case ValuesSet::ATTRIBUTE_FLOAT:
		ss << get_flt();
		break;
	case ValuesSet::ATTRIBUTE_INT:
		ss << get_int();
		break;
	case ValuesSet::ATTRIBUTE_STR:
		ss << get_str();
		break;
	default:break;
	}
	return ss.str();
}
