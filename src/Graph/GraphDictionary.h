/*
 * GraphDictionary.h
 *
 *  Created on: 1 oct. 2012
 *      Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 *  Edit on : 04 juin 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#ifndef GraphDictionary_H_
#define GraphDictionary_H_

#include "Term.h"

#include <string>
#include <list>
#include <map>

using namespace std;

class Term;
/**
 * On utilise une liste de pointeur pour conserver des pointeurs
 * sur les termes (objets pointé de l'extérieur), et facilement
 * recopiable.
 */


/**
 * \class GraphDictionary
 * Un dictionnaire de Labels est une structure de données servant à contenir
 * l'ensemble des valeurs qui peuvent prise par des Labels.
 * Les valeurs sont retenus sont la forme d'attributs/valeurs. Donc la structure
 * est principalement une map avec pour entrée le nom de l'attribut (un Attribute).
 *
 * Les valeurs sont appelés des termes.
 *
 *  \see Attribute, Values
 */
class GraphDictionary : public map<string, ValuesSet> {
protected:
	string _attributeUsed; //Désigne l'attribut qui sert de classe pour les objets (noeud ou arcs)
public:
	GraphDictionary();
	GraphDictionary(const GraphDictionary &);
	virtual ~GraphDictionary(){clean();};

	/**
	 * Suppression de la mémoire
	 */
	void clean();

	/**
	 * \brief Ajout d'une nouvelle valeur val à l'attribut attr
	 */
	Term *addValue(string attr, string val);

	/**
	 * \brief Ajout d'une nouvelle valeur val à l'attribut attr
	 */
	Term *addValue(string attr, int val);

	/**
	 * \brief Ajout d'une nouvelle valeur val à l'attribut attr
	 */
	Term *addValue(string attr, float vals);


	/**
	 * \brief création d'un attribut avec un type
	 */
	GraphDictionary::iterator addAttribute(string attr, ValuesSet::AttributeType type);

	string used() const {return _attributeUsed;};
	void setUsed(string val) {_attributeUsed=val;};

	const ValuesSet* usedValueSet() const {
		if( find(_attributeUsed)!=end()) {
			return &at(_attributeUsed);
		}
		return NULL;
	};

	bool hasAttribute(string attr) const;

	/**
	 * \brief Création de deux disctionnaire (nodes et edges) à partir d'un fichier de graphe
	 */
	static pair<GraphDictionary *, GraphDictionary *> readFromGraph(ifstream& source);
};

/**
 * Fonction d'affichage
 */
ostream& operator<<(ostream& o, const GraphDictionary& ont);

#endif /* GraphDictionary_H_ */
