/*
 * Graph.cpp
 *
 *  Created on: 24 mai 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *      Author: Thomas Guyet
 *  Edit on: 04 juin 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#include "Graph.h"

#include <cmath>

using namespace std;

class hsv {
public:
    double h;       // angle in degrees
    double s;       // percent
    double v;       // percent
};

/*
*Graph Constructor/Destructor
*/

#define clearList(x) while(!x.empty())delete x.front(),x.pop_front();

Graph::Graph() {
	nodeDictionary=new GraphDictionary();
	edgeDictionary=new GraphDictionary();
}

Graph::Graph(GraphDictionary* nodeDict,GraphDictionary* edgeDict) {
	nodeDictionary = new GraphDictionary(*nodeDict);
	edgeDictionary = new GraphDictionary(*edgeDict);
}


Graph::~Graph() {
	clearList(nodes);
	clearList(edges);

	delete nodeDictionary;
	delete edgeDictionary;
}


/*
*Graph Get/Set
*/
void Graph::setNodes(const list<Node*>& nodes) {
	clearList(this->nodes);

	this->addNodes(nodes);
}


Node* Graph::getNode(int id)const {
	Node* n=NULL;
	list<Node*>::const_iterator it=nodes.begin();
	while(it!=nodes.end() && n==NULL){
		if((*it)->getID()==id){
			n=*it;
		}
		it++;
	}
	return n;
}

Edge* Graph::getEdge(int id)const {
	Edge* e=NULL;
	list<Edge*>::const_iterator it=edges.begin();
	while(it!=edges.end() && e==NULL){
		if((*it)->getID()==id){
			e=*it;
		}
		it++;
	}
	return e;
}

void Graph::setEdges(const list<Edge*>& edges) {
	clearList(this->edges);

	this->addEdges(edges);
}

/*
*Graph operations
*/
void Graph::addNodes(const list<Node*>& nodes) {
	for(Node* n:nodes){
		addNode(n);
	}
}

void Graph::addNode(Node* node) {
	nodes.push_back(node);
}

void Graph::addEdges(const list<Edge*>& edges) {
	for(Edge* e:edges){
		addEdge(e);
	}
}

void Graph::addEdge(Edge* edge) {
	edges.push_back(edge);
}


/*
*Graph Display
*/
string Graph::toString() const {
	stringstream s;
	s<<"Graph : "<<nodes.size()<<" nodes, "<<edges.size()<<" edges.";
	return s.str();
}

void Graph::print(ostream& o) const {
	o<<write();
}

void Graph::printTGE(ostream &o) const {
	stringstream output;

	std::map<int, list<int> > matrix;
	for(const Edge* edge:edges) {
		if( edge->getNodeA()->getID()< edge->getNodeB()->getID() ) {
			matrix[edge->getNodeA()->getID()].push_back(edge->getNodeB()->getID());
		} else {
			matrix[edge->getNodeB()->getID()].push_back( edge->getNodeA()->getID() );
		}
	}

	int i=0;
	for(pair<int, list<int>> p: matrix) {
		while(p.first>i) {
			o <<endl;
			i++;
		}
		//ICI p.first==i
		list<int>::iterator itb=p.second.begin();
		while( itb!=p.second.end() ) {
			o << (*itb);
			itb++;
			if( itb!=p.second.end() )
				o << ",";
		}
		o <<endl;
		i++;
	}
}

ostream& operator <<(ostream& o, const Graph& g) {
	g.print(o);
	return o;
}

void Graph::draw(string destination, string format) const {

	stringstream graph;
	graph<<"graph G {"<<endl;

	graph<<"node[style=filled]"<<endl;

	for (const Node* node:nodes) {
		unsigned int colorval=node->getLabels()->getMainLabel()->id()+1;
		hsv color;
		if( nodeDictionary->usedValueSet()!=NULL) {
			color.h= (360.0*(double)colorval)/((double)nodeDictionary->usedValueSet()->size()+1);
		} else {
			color.h= (360.0*(double)colorval)/(double)25;
		}
		color.s=1.0;
		color.v=1.0;
		stringstream ss;
		ss << "color=\""<<color.h/360.0<<" "<<color.s<<" "<<color.v<<"\"";

		graph<<node->getID()
						<<"[" << ss.str() << ", Label=\""
						<< *( node->getLabels()->getMainLabel())
						<< "\"];"
						<<endl;

	}

	for (const Edge* edge:edges) {
		graph<<(edge->getNodeA())->getID()
						<< " -- "
						<< (edge->getNodeB())->getID();
		if(edge->getLabels()->getMainLabel()!=NULL ) {
			graph << "[Label=\""
						<< *(edge->getLabels()->getMainLabel())
						<< "\", color=\"black\"]";
		}
		graph << ";" << endl;
	}

	graph<<"}"<<endl;

	ofstream temp(destination+".dot");
	temp<<graph.str();
	temp.close();

	stringstream s;
	s<<"dot -T"+format+" "+destination+".dot -o "<<destination+"."+format;
	system (s.str().c_str());

	remove((destination+".dot").c_str());

}

/*
*Graph Read/Write
*/
Graph* Graph::read(const string& source) {
	ifstream reader(source.c_str());

	Graph* graph=new Graph();
	read(reader,*graph);
	reader.close();

	if(!graph->getEdges().size() && !graph->getNodes().size()) {
		delete graph;
		graph=NULL;
	}

	return graph;
}

void Graph::read(ifstream& reader, Graph& graph) {

	string line;

	if (reader.is_open()){

		//set the ontologies to nullptr, they will be read next
		delete graph.nodeDictionary;
		graph.nodeDictionary=NULL;
		delete graph.edgeDictionary;
		graph.edgeDictionary=NULL;

		pair<GraphDictionary*,GraphDictionary*> dictionaries=GraphDictionary::readFromGraph(reader);

		if( dictionaries.first==NULL || dictionaries.second==NULL ) {
			cerr << "ERROR Graph::read: dictionaries construction from graph failed"<<endl;
			return;
		}
		graph.nodeDictionary=dictionaries.first;
		graph.edgeDictionary=dictionaries.second;

		//On remonte au début du fichier
		reader.clear();
		reader.seekg(0,ios::beg);

		while(reader.good()){
			getline(reader,line);
			if(line.size()==0) {
				continue;
			}
			//read node
			if(line.compare(0,1,"v")==0){
				int nextSpace=line.find_first_of(" ", 2);

				Node* node=new Node();
				node->setID(atoi(line.substr(2, nextSpace-2).c_str()));

				int xStart=line.find_first_of("(", nextSpace);
				int xEnd=line.find_first_of(",", xStart);
				node->setX( atof(line.substr(xStart+1, xEnd-1).c_str()) );

				int yEnd=line.find_first_of(")", xEnd);
				node->setY( atof(line.substr(xEnd+1, yEnd-1).c_str()) );

				Label *lab=new Label(line.substr(yEnd+1),graph.getNodeDictionary());
				if( lab==NULL ) {
					cerr << "ERROR null Label"<<endl;
				}
				node->setLabels( lab );
				graph.addNode(node);
			}

			//read edge
			if(line.compare(0,1,"u")==0){
				int nextSpace=line.find_first_of(" ", 2);
				int lastSpace=line.find_first_of(" ", nextSpace+1);
				int idA=atoi(line.substr(2, nextSpace-2).c_str());
				int idB=atoi(line.substr(nextSpace+1, lastSpace-nextSpace).c_str());

				Edge* edge=new Edge(graph.getNode(idA),graph.getNode(idB));
				Label *lab=new Label(line.substr(lastSpace+1), graph.getEdgeDictionary());
				if( lab==NULL ) {
					cerr << "ERROR null Label"<<endl;
				}
				edge->setLabels( lab );
				graph.addEdge(edge);
			}
		}
	} else {
		cerr << "ERROR : error while opening graph !!" << endl;
	}
}

string Graph::lematize(string s) {
	size_t index=s.find(" ");
	while(index!=string::npos){
		s.replace(index,1,"_");
		index=s.find(" ");
	}
	return s;
}

string Graph::write() const {
	stringstream output;

	for(const Node* node:nodes) {
		output << "v " << node->getID() << " ("<<node->X()<<","<<node->Y()<<") "<< Graph::lematize(node->getLabels()->toStringAll()) <<endl;
	}

	for(const Edge* edge:edges) {
		output << "u " << edge->getNodeA()->getID() << " " << edge->getNodeB()->getID() << " " << Graph::lematize(edge->getLabels()->toStringAll())<<endl;
	}

	return output.str();
}

/*
*Graph Comparator
*/

bool Graph::operator==(const Graph& other) const {

	if(nodes.size()!= other.nodes.size() || edges.size()!= other.edges.size() ){
		return false;
	}

	for (const Edge* edge:edges) {
		Edge* otherEdge = other.getEdge(edge->getID());

		if(otherEdge == NULL || !edge->compareToByID(*otherEdge)){
			return false;
		}
	}
	for (const Node* node:nodes) {
		Node* otherNode = other.getNode(node->getID());

		if(otherNode == NULL || !node->getLabels()->equalLabels( *(otherNode->getLabels()) )){
			return false;
		}
	}

	return true;
}




