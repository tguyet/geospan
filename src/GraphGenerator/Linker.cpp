/*
 * Linker.cpp
 *
 *  Created on: 4 juil. 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#include "Linker.h"
#include "MetaData.h"

Linker::Linker() {
	links=map<string,GenericShapeTypes>();

}

Linker::~Linker() {
}

GenericShapeTypes Linker::findType(const MetaData& meta) const {
	GenericShapeTypes type;

	map<string,GenericShapeTypes>::const_iterator res = links.find(meta.getRef());

	if( res==links.end() ){
		type = GenericShapeTypes::NO_DATA;
	} else {
		type = res->second;
	}

	return type;
}

SimpleLinker::SimpleLinker(string refField,string idField, GenericShapeTypes type) {
	this->refField=refField;
	this->idField=idField;
	this->type=type;
}

SimpleLinker::~SimpleLinker() {
}



