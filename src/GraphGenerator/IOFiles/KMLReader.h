/*
 * KMLReader.h
 *
 *  Created on: 1er juillet 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */
#ifndef KMLREADER_H_
#define KMLREADER_H_

#include <iostream>
#include <string>
#include <stdlib.h>

class KMLReader
{
private:
	int depth;
	std::string fileName = "";
public:
	/**
	*	Constructor/Destructor
	*/
	KMLReader();
	KMLReader(int depth_);
 	KMLReader(std::string inputFileName);
	~KMLReader(){};

	/* data */
	void inputKMLFile();

	/**
	*	Getter/Setter
	*/

	int getDepth() const {
		return depth;
	}

	void setDepth(int depth) {
		this->depth = depth;
	}

	std::string getFileName() const {
		return fileName;
	}

	void setFileName(std::string fileName = "") {
		this->fileName = fileName;
	}
};

std::ostream & operator<<(std::ostream & o, KMLReader const & in);

#endif
