/*
 * KMLWriter.cpp
 *
 *  Created on: 30 juin 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#include "KMLWriter.h"

KMLWriter::KMLWriter(): fileName(""), streamFileName(""), idVersion(0), EPSG(LambertZoneII) {}

KMLWriter::KMLWriter(std::string name, double idversion, int epsg){
  if(name != "" || name != fileName) fileName = name;
  fileName = name + ".xml";

  if(idversion > idVersion) idVersion = idversion;
  if(epsg != EPSG) EPSG = epsg;
}

KMLWriter::~KMLWriter() {
  std::ofstream streamFileName(fileName, std::ios::app);
  streamFileName << "</graph>" << std::endl;
  streamFileName << "</kml>" << std::endl;
  streamFileName.close();
}

void KMLWriter::Init(){
   std::ofstream streamFileName(fileName);
  if ( !streamFileName ) 
  { 
    std::cerr << "Erreur de création\n"; 
    return; 
  }else{
    streamFileName << "<kml xmlns=" << "http://earth.google.com/kml/2.1" << ">" << std::endl;
    streamFileName << "<graph=" << getFileName() << "\tversion :" << getIdVersion() << "\tEPSG :" << getEpsg() <<">" << std::endl;
  }
}

void KMLWriter::WriteGeometries(double x, double y) {
  std::ofstream streamFileName(fileName, std::ios::app);
  streamFileName <<  "<coordinate>"<< std::endl;
  streamFileName <<  "x = " << x << ", y = " << y << std::endl; 
  streamFileName <<  "</coodinate>"<< std::endl;
  

    if ( ! streamFileName ) 
    { 
        std::cerr << "Erreur d'écriture\n"; 
        return; 
    } 
    std::cout << "L'écriture a réussi\n"; 
    streamFileName.close();
}


void KMLWriter::WriteFeatures(std::string st) {
  std::ofstream streamFileName(fileName, std::ios::app);
  streamFileName << "<Features>" << std::endl; 
  streamFileName << st << std::endl; 
  streamFileName << "</Features>" << std::endl; 
  streamFileName <<  "</Node>"<< std::endl;

    if ( ! streamFileName ) 
    { 
        std::cerr << "Erreur d'écriture\n"; 
        return; 
    } 
    std::cout << "L'écriture a réussi\n"; 
    streamFileName.close();
}

void KMLWriter::WriteNode(int i) {
  std::ofstream streamFileName(fileName, std::ios::app);
  streamFileName << "<Node id=" << i <<">" << std::endl; 
    if ( ! streamFileName ) 
    { 
        std::cerr << "Erreur d'écriture\n"; 
        return; 
    } 
    std::cout << "L'écriture a réussi\n"; 
    streamFileName.close();
}

void KMLWriter::WriteEdge(int i, int from, int to) {
  std::ofstream streamFileName(fileName, std::ios::app);
  streamFileName << "<Edge id=" << i <<">" << std::endl;
  streamFileName << "From Node : " << from <<"\tTo Node :" << to << std::endl;
  streamFileName << "</Edge>" << std::endl;

    if ( ! streamFileName ) 
    { 
        std::cerr << "Erreur d'écriture\n"; 
        return; 
    } 
    std::cout << "L'écriture a réussi\n"; 
}


