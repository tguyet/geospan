/*
 * KMLReader.cpp
 *
 *  Created on: 22 juin 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */
#include "KMLReader.h"

KMLReader::KMLReader(): depth(0), fileName("") {}

KMLReader::KMLReader(int depth_): depth(depth_), fileName("") {}

KMLReader::KMLReader(std::string inputFileName): depth(0), fileName(inputFileName) {}

void KMLReader::inputKMLFile(){
	std::string inputFileName = "";

	while(inputFileName == ""){
		std::cout << "Entrez le fichier à extraire" << std::endl;
		std::cin >> inputFileName;

		if (inputFileName == "")
		{
			std::cerr << "Wrong name file, try again" << std::endl;
			exit(1);
		}
		else{
			this->setFileName(inputFileName);
		}
	}
}

std::ostream & operator<<(std::ostream & o, KMLReader const & in)
{
	for(int i = 0; i != in.getDepth(); ++i)
	{
		o << "  ";
	}
	return o;
}
