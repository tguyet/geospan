/*
 * OGRReader.cpp
 *
 *  Created on: 22 juin 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */
#include "OGRReader.h"

#include <string>
#include <iostream>
#include <sstream>

using namespace std;

/**
 *	Constructor/Destructor
 */

/**
 *	Initially it is necessary to register all the format drivers that are desired.
 */

OGRReader::OGRReader(const FileWrapper &file){
	const char * name = (file.path).c_str(); 
 	fileName = name;
 	poDs = nullptr;
	GDALAllRegister();
}

OGRReader::~OGRReader(){
	GDALClose( poDs );
}


/**
 *	OpeningFile
 */
void OGRReader::OpeningFile(OGRLayer * poLayer){

	while(poLayer == nullptr){
		
 		if(getFileName() == NULL)
 		{
 			std::cerr << "Wrong name file, try again" << std::endl;
 		} else{
 			setPoDs((GDALDataset*) GDALOpenEx( getFileName(), GDAL_OF_VECTOR, NULL, NULL, NULL ));
			if( getPoDs() != NULL )
			{
				poLayer = getPoDs()->GetLayer(0);
	 			std::cerr << "Open Succed.\n" << std::endl;
				
			}else{
				std::cerr << "Open failed.\n" << std::endl;
				exit( 1 );
	 		}
 		}
	}
}

/**
 *	Fetching Data
 */


/**
 *	In order to dump all the attribute fields of the feature, it is helpful to get the OGRFeatureDefn.
 *	This is an object, associated with the layer, containing the definitions of all the fields.
 *	We loop over all the fields, and fetch and report the attributes based on their type.
 */
MetaData* OGRReader::fetchFeatures(OGRFeature *poFeature, MetaData* poMetadata) {
	OGRFeatureDefn *poFDefn = getPoDs()->GetLayer(0)->GetLayerDefn();
	int iField;

	for( iField = 0; iField < poFDefn->GetFieldCount(); iField++ )
	{
		OGRFieldDefn *poFieldDefn = poFDefn->GetFieldDefn( iField );

		if( poFieldDefn->GetType() == OFTInteger ){
			poMetadata->setIntegerMetaData(string(poFieldDefn->GetNameRef()),poFeature->GetFieldAsInteger(iField));
		}
		else if( poFieldDefn->GetType() == OFTReal ){
			poMetadata->setDoubleMetaData(string(poFieldDefn->GetNameRef()),poFeature->GetFieldAsDouble(iField));
		}
		else if( poFieldDefn->GetType() == OFTString ){
			poMetadata->setStringMetaData(string(poFieldDefn->GetNameRef()),string(poFeature->GetFieldAsString(iField)));
		}
		else{
			poMetadata->setStringMetaData(string(poFieldDefn->GetNameRef()),string(poFeature->GetFieldAsString(iField)));
		}
	}
	return poMetadata;
}






