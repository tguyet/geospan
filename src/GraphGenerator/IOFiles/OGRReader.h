/*
 * OGRReader.h
 *
 *  Created on: 22 juin 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */
#ifndef OGRREADER_H_
#define OGRREADER_H_

#include "gdal/ogrsf_frmts.h"
#include "gdal/gdal_priv.h"
#include "gdal/gdal.h"
#include <map>

#include "GraphGenerator/MetaData.h"


/**
 * Structure wrapping together the path were to read the
 * shapes and the linker to use for this file.
 */
 struct FileWrapper {
	string path;
	Linker* linker;
	FileWrapper(string p, Linker* l):path(p),linker(l){}
};

class OGRReader
{
private:
	const char * fileName;
	OGREnvelope envelope;
	GDALDataset	* poDs;
	/* data */
public:

	/**
	*	Constructor/Destructor
	*/

	 /**
	 *	Initially it is necessary to register all the format drivers that are desired.
	 */
	OGRReader(const FileWrapper &file);

	~OGRReader();


	/**
	*	OpeningFile
	*/
	
	/**
	 *	we need to open the input OGR datasource
	 *	OGRLayer * poLayer
	 */
	void OpeningFile(OGRLayer * poLayer);

	/**
	*	Fetching Data
	*/

	/**
	 *	In order to dump all the attribute fields of the feature, it is helpful to get the OGRFeatureDefn.
	 *	This is an object, associated with the layer, containing the definitions of all the fields.
	 *	We loop over all the fields, and fetch and report the attributes based on their type.
	 */
	MetaData* fetchFeatures(OGRFeature *poFeature, MetaData* poMetadata);

	const OGREnvelope& getEnvelope() const {return envelope;}	
	/**
	*	Getter/Setter
	*/


	GDALDataset * getPoDs() const {
		return poDs;
	}

	void setPoDs(GDALDataset * poDs) {
		this->poDs = poDs;
	}

	const char* getFileName() const {
		return fileName;
	}

	void setFileName(const char* fileName) {
		this->fileName = fileName;
	}
};

#endif
