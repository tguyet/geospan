/*
 * KMLWriter.cpp
 *
 *  Created on: 30 juin 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */
#ifndef KMLWRITER_H_
#define KMLWRITER_H_ 
 
#include "gdal/ogrsf_frmts.h"
#include "gdal/gdal_priv.h"
#include "gdal/gdal.h"

#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>


#include <iostream>
#include <sstream>
#include <fstream>
#include <string>


#define LambertZoneII 27572

 class KMLWriter
 {
 private:
 	std::string fileName = "";
 	std::ostringstream streamFileName;
 	double idVersion = 0;
 	int EPSG = LambertZoneII;
 public:
 	/**
 	*	Constructor/Destructor
 	*/
 	KMLWriter();
 	KMLWriter(std::string name, double idversion, int epsg);
 	~KMLWriter();
 
 	/* data */
 	/**
 	*	FormatPlacemark:
 	*	write data in a kml format file
 	*/
 	void Init();

 	void WriteFeatures(std::string st);

 	void WriteNode(int i);

 	void WriteEdge(int i, int from, int to);

 	void WriteGeometries(double x, double y);

 	/**
 	*	Getter/Setter
 	*/
	int getEpsg() const {
		return EPSG;
	}

	void setEpsg(int epsg = LambertZoneII) {
		EPSG = epsg;
	}

	std::string getFileName() const {
		return fileName;
	}

	void setFileName(std::string fileName) {
		this->fileName = fileName;
	}

	double getIdVersion() const {
		return idVersion;
	}

	void setIdVersion(double idVersion = 0) {
		this->idVersion = idVersion;
	}

	const std::ostringstream& getStreamFileName() const {
		return streamFileName;
	}

};


#endif
