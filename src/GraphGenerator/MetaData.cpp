/*
 * MetaData.cpp
 *
 *  Created on: 4 juil. 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#include "MetaData.h"
#include <limits>


using std::numeric_limits;
using std::to_string;
using std::pair;

int MetaData::idGenerator = 0;


MetaData::MetaData(const Linker* linker) {
	odata_double=map<string, double>();
	odata_integer=map<string, int>();
	odata_string=map<string, string>();

	this->linker=linker;

	shapeID=idGenerator++;
}

MetaData::MetaData(const MetaData& other) {
	odata_double=map<string, double>(other.odata_double);
	odata_integer=map<string, int>(other.odata_integer);
	odata_string=map<string, string>(other.odata_string);

	this->linker=other.linker;
}

MetaData::~MetaData() {
}

int MetaData::getIntegerMetaData(string name) const {
	int res=numeric_limits<int>::quiet_NaN();
	map<string,int>::const_iterator it= odata_integer.find(name);
	if(it!=odata_integer.end()){
		res=it->second;
	}

	return res;
}

void MetaData::setIntegerMetaData(string name, int value) {
	odata_integer[name]=value;
}

double MetaData::getDoubleMetaData(string name) const {
	double res=numeric_limits<double>::quiet_NaN();

	map<string,double>::const_iterator it= odata_double.find(name);
	if(it!=odata_double.end()){
		res=it->second;
	}

	return res;
}

void MetaData::setDoubleMetaData(string name, double value) {
	odata_double[name]=value;
}

string MetaData::getStringMetaData(string name) const {
	string res="";

	map<string,string>::const_iterator it= odata_string.find(name);
	if(it!=odata_string.end()){
		res=it->second;
	}

	return res;
}

void MetaData::setStringMetaData(string name, string value) {
	odata_string[name]=value;
}

string MetaData::getMetaData(string name) const {
	string res="";

	int i=getIntegerMetaData(name);
	if(i!=numeric_limits<int>::quiet_NaN()){
		res=to_string(i);
	}else{
		double d=getDoubleMetaData(name);
		if(d!=numeric_limits<double>::quiet_NaN()){
			res=to_string(d);
		}else{
			res=getStringMetaData(name);
		}
	}
	return res;
}


MetaDataType MetaData::hasMetaData(string name) const {

	if(getIntegerMetaData(name)!=numeric_limits<int>::quiet_NaN()){
		return INT;
	}else if(getDoubleMetaData(name)!=numeric_limits<double>::quiet_NaN()){
		return DOUBLE;
	}else if(getStringMetaData(name)!=""){
		return STRING;
	}
	return NOTFOUND;
}

int MetaData::getId()const{
	return shapeID;
}


string MetaData::getShapeId() const {
	return getMetaData( linker->getIdField() );
}

string MetaData::getRef()const{
	return getMetaData( linker->getRefField() );
}



