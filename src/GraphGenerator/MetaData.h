/*
 * MetaData.h
 *
 *  Created on: 4 juil. 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#ifndef METADATA_H_
#define METADATA_H_

#include <list>
#include <string>
#include <map>

#include "Linker.h"

using std::map;
using std::string;

using namespace std;

/**
 * Attribute types
 */
enum MetaDataType {INT, DOUBLE, STRING, NOTFOUND};

/*
 * \class MetaData
 * \brief Attribute associated to shapes in draft landscapes
 * \author Antoine Despres
 * \see Shape
 */
class MetaData {

private:
	/** Used to generate id's if no id field exist */
	static int idGenerator;
	int shapeID;
	const Linker* linker;

	map<string, int> odata_integer;
	map<string, double> odata_double;
	map<string, string> odata_string;

public:
	MetaData(const Linker* linker);
	MetaData(const MetaData& other);
	virtual ~MetaData();

	int getIntegerMetaData(string name)const;
	void setIntegerMetaData(string name, int value);
	double getDoubleMetaData(string name)const;
	void setDoubleMetaData(string name, double value);
	string getStringMetaData(string name)const;
	void setStringMetaData(string name, string value);
	string getMetaData(string name)const;

	const map<string,int>& getIntegerData() const {return odata_integer;}
	const map<string,double>& getDoubleData() const {return odata_double;}
	const map<string,string>& getStringData() const {return odata_string;}

	MetaDataType hasMetaData(string name)const;

	int getId()const;

	GenericShapeTypes getType() const {return linker->findType(*this);};


	string getShapeId()const;

	string getRef()const;

};

#endif /* METADATA_H_ */
