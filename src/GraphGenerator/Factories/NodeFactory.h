/*
 * NodeFactory.cpp
 *
 *  Created on: 10 juillet 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */
 
#ifndef NODEFACTORY_H	
#define NODEFACTORY_H


#include <iostream>
#include <string>
#include <list>

#include "GraphGenerator/IOFiles/KMLWriter.h"
#include "GraphGenerator/MetaData.h"
#include "Graph/Graph.h"
#include "Graph/Node.h"
#include "Graph/Label.h"

#include "gdal/ogrsf_frmts.h"
#include "gdal/gdal_priv.h"

class NodeFactory
{
private:
	int idNode;
	OGRGeometry* poGeometry;
public:
	NodeFactory();
	NodeFactory(OGRGeometry * Geometry, int id);
	~NodeFactory();

	/* data */

	void NodeGenerator(Graph* graph, Label * l);

	Label FeaturesGenerator(MetaData *poMetaData);

	OGRGeometry* getPoGeometry() const {
		return poGeometry->clone();
	}

	int getIdNode() const {
		return idNode;
	}

	void setIdNode(int idNode) {
		this->idNode = idNode;
	}
};


#endif
