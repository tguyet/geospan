/*
 * GraphFactory.cpp
 *
 *  Created on: 22 juin 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#include "GraphFactory.h"

GraphFactory::GraphFactory(){
	graph = new Graph();
	ogrr = nullptr;
	envelope=OGREnvelope();
}


GraphFactory::~GraphFactory(){
	/**
	 *	we do need to delete the datasource in order to close the input file
	 */
	delete(ogrr);
}

void GraphFactory::FetchLayer(const FileWrapper &file){
	NodeFactory * nodeFactory = new NodeFactory();
	EdgeFactory * edgeFactory = new EdgeFactory();
	OGRFeature * poFeature = nullptr;
	OGRLayer * poLayer = nullptr;
	int idNode = 0;
	/**
	 *	Initially it is necessary to register all the format drivers that are desired.
	 */
	ogrr = new OGRReader(file);
	envelope = ogrr->getEnvelope();
	/**
	 *	Opening your source file
	 */
	ogrr->OpeningFile(poLayer);

	/**
	 *	The number of layers available can be queried with GDALDataset::GetLayerCount()
	 *	and individual layers fetched by index using GDALDataset::GetLayer().
	 * 	However, we will just fetch the layer by name.
	 */
	poLayer = ogrr->getPoDs()->GetLayer(0);

	/**
	 *	it is often wise to call OGRLayer::ResetReading() to ensure we are starting at the beginning of the layer.
	 *	We iterate through all the features in the layer using OGRLayer::GetNextFeature().
	 *	It will return NULL when we run out of features.
	 */
	poLayer->ResetReading();

	while( (poFeature = poLayer->GetNextFeature()) != NULL )
	{
		MetaData* poMetadata = new MetaData(file.linker);
		/**
		 *	In order to dump all the attribute fields of the feature, it is helpful to get the OGRFeatureDefn.
		 *	This is an object, associated with the layer, containing the definitions of all the fields.
		 *	We loop over all the fields, and fetch and report the attributes based on their type.
		 */
		ogrr->fetchFeatures(poFeature,poMetadata);


		Label label = Label(nodeFactory->FeaturesGenerator(poMetadata));
		/**
		 *	Geometries are returned as a generic OGRGeometry pointer.
		 *	We then determine the specific geometry type, and if it is a point, we cast it to point and operate on it.
		 *	If it is something else we write placeholders.
		 */
		nodeFactory = new NodeFactory(poFeature->GetGeometryRef(), idNode);
		Node* node = new Node(nodeFactory->getPoGeometry(), label);
		graph->addNode(node);

		nodeFactory->NodeGenerator(graph, &label);
		nodeFactory->setIdNode(idNode++);

		/**
		 *	Note that OGRFeature::GetGeometryRef() and OGRFeature::GetGeomFieldRef() return a pointer to the internal geometry owned by the OGRFeature.
		 *	To be on the safe side we use a GDAL function to delete the feature.
		 */
		OGRFeature::DestroyFeature( poFeature );
	}
	edgeFactory->EdgeGenerator(graph);
}

