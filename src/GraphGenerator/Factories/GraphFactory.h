/*
 * GraphFactory.h
 *
 *  Created on: 10 juillet 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

 #ifndef GRAPHFACTORY_H
 #define GRAPHFACTORY_H

class MetaData;
struct FileWrapper;

#include "gdal/ogrsf_frmts.h"
#include "gdal/gdal_priv.h"

#include "Graph/Graph.h"
#include "Graph/Node.h"
#include "NodeFactory.h"
#include "EdgeFactory.h"
#include "GraphGenerator/MetaData.h"
#include "GraphGenerator/IOFiles/KMLWriter.h"
#include "GraphGenerator/IOFiles/OGRReader.h"


#include <iostream>
#include <string>
#include <map>

 class GraphFactory
 {
 private:
 	Graph *graph;
	OGREnvelope envelope;
	OGRReader *ogrr;
 public:
 	GraphFactory();
 	~GraphFactory();
 
 	/* code */

 	void FetchLayer(const FileWrapper &file);

 	/*	Getter	*/

	void init();

	const OGREnvelope& getEnvelope() const {
		return ogrr->getEnvelope();
	}

	Graph* getGraph() const {
		return graph;
	}
};
 #endif
