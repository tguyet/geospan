/*
 * NodeFactory.cpp
 *
 *  Created on: 10 juillet 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */
 */
#include "NodeFactory.h"

NodeFactory::NodeFactory(){
	idNode = 0;
	poGeometry = nullptr;
}

NodeFactory::NodeFactory(OGRGeometry * Geometry, int id){
	idNode = id;
	poGeometry = Geometry;
}

NodeFactory::~NodeFactory(){
	delete(poGeometry);
}

void NodeFactory::NodeGenerator(Graph* graph, Label *label){
	poGeometry = nullptr;
	OGRPoint *poPoint = nullptr;
	KMLWriter * kmlw = new KMLWriter("test", 1, LambertZoneII);
	/* code */
	Node* node = graph->getNode(idNode);
		unsigned char * wkbPoint = node->getWkbPoint();

		std::string stNode = "idNode :\t " + to_string(idNode);
		std::cout << stNode << std::endl;
		kmlw->WriteNode(idNode);

		if(OGRGeometryFactory::createFromWkb(wkbPoint, NULL, &poGeometry) != OGRERR_NONE){
			std::cerr << "Importing a Wkb polygon failed." << std::endl;
			exit(1);
		}else{
			poPoint = new OGRPoint();

			if(poGeometry != nullptr){
				poPoint = (OGRPoint *) poGeometry;
				std::cout << "Centroid :\t " << "("<<poPoint->getX() << "," << poPoint->getY() << ")"<< std::endl;
				kmlw->WriteGeometries(poPoint->getX(),poPoint->getY());
				kmlw->WriteFeatures(label->toString());
			}
		}
}

Label NodeFactory::FeaturesGenerator(MetaData *poMetadata){
		GraphDictionary * graphDictionary =  new GraphDictionary();
		ValuesSet * valuesSet = new ValuesSet();
		Label * label;
		Term * term = nullptr;
		std::string stringData = "";
		int iData =0;
		float fData = 0;
		std::string strData = "";
		stringstream str;
		int inserted=0;

		str<<"{";

		//Label
		const map<string, double> dblatt = poMetadata->getDoubleData();
		map<string, double>::const_iterator it= dblatt.begin();
		while(it!=dblatt.end()) {
			str << (*it).first <<"="<< (*it).second;
			stringData = (*it).first;
			term = new Term((float)(*it).second );
			valuesSet->push_back(term);
			valuesSet->setType(ValuesSet::ATTRIBUTE_INT);
			iData = term->get_int();
			term = graphDictionary->addValue(stringData, iData);
			it++;
			inserted=1;
			if( it!=dblatt.end() ) str << ",";
		}


		const map<string, int> intatt = poMetadata->getIntegerData();
		if(intatt.size()>0) {
			if(inserted) str <<",";
			map<string, int>::const_iterator it= intatt.begin();
			while(it!=intatt.end()) {
				str << (*it).first <<"="<< (*it).second;
				stringData = (*it).first;
				term = new Term( (*it).second );
				valuesSet->push_back(term);
				valuesSet->setType(ValuesSet::ATTRIBUTE_FLOAT);
				fData = term->get_flt();
				term = graphDictionary->addValue(stringData, fData);
				it++;
				inserted=1;
				if( it!=intatt.end() ) str << ",";
			}
		}

		const map<string, string> stratt = poMetadata->getStringData();
		if(stratt.size()>0) {
			if(inserted) str <<",";
			map<string, string>::const_iterator it= stratt.begin();
			while(it!=stratt.end()) {
				str << (*it).first <<"="<< (*it).second;
				stringData = (*it).first;
				term = new Term( (*it).second );
				valuesSet->push_back(term);
				valuesSet->setType(ValuesSet::ATTRIBUTE_STR);
				strData = term->get_str();
				term = graphDictionary->addValue(stringData, fData);
				it++;
				if( it!=stratt.end() ) str << ",";
			}
		}
		str<<"}";
		label = new Label(str.str(), graphDictionary);
		std::cout << "Label" << label->toString() << std::endl;
		return *label;
}
