/*
 * EdgeFactory.cpp
 *
 *  Created on: 10 juillet 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */
#include "EdgeFactory.h"

EdgeFactory::EdgeFactory(){
	poGeometry = nullptr;
	neighborGeometry = nullptr;
	idEdge = 0;
	idNode = 0;
}

EdgeFactory::~EdgeFactory(){
	delete(poGeometry);
	delete(neighborGeometry);
}

void EdgeFactory::EdgeGenerator(Graph * graph){
	KMLWriter * kmlw = new KMLWriter("test", 1, LambertZoneII);
	list<Node*> nodes = graph->getNodes();
	list<Node*>::iterator it = nodes.begin();

	while(it != nodes.end()) {
		unsigned char * wkbPoint = (*it)->getWkbPoint();
		if(OGRGeometryFactory::createFromWkb(wkbPoint, NULL, &poGeometry) != OGRERR_NONE){
			std::cerr << "Importing a Wkb polygon failed." << std::endl;
			exit(1);
		}
		else{
			list<Node*>::iterator jt = it;
			int idNeighbor =idNode;
			jt++;
			idNeighbor++;
			while(jt != nodes.end()) {
				unsigned char * neighborWkbPoint = (*jt)->getWkbPoint();

				if(OGRGeometryFactory::createFromWkb(neighborWkbPoint, NULL, &neighborGeometry) != OGRERR_NONE){
					std::cerr << "Importing a Wkb polygon failed." << std::endl;
					exit(1);
				}
				else{
					if( (neighborGeometry != nullptr || poGeometry != nullptr)) {
						if(neighborGeometry->Touches(poGeometry) ) {
							std::string st = "idEdge :\t " + to_string(idEdge) + "\n" + "From Node " + to_string(idNode) + "\t" + " to Node " + to_string(idNeighbor);
							std::cout <<  st << "\n"<<std::endl;
							kmlw->WriteEdge(idEdge, idNode, idNeighbor);
							Edge * edge = new Edge( (*it), (*jt) );
							(*it)->add(edge);
							graph->addEdge(edge);

							idEdge++;
						}
					}
				}	
				jt++;
				idNeighbor++;	
			}
		}
		it++;
		idNode++;
	}
}
