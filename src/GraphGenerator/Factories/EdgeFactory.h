/*
 * EdgeFactory.h
 *
 *  Created on: 10 juillet 2015
 *	   Author : Marec Frederic (ddmarec@gmail.com)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

 #ifndef EDGEFACTORY_H
 #define EDGEFACTORY_H

#include "gdal/ogrsf_frmts.h"
#include "gdal/gdal_priv.h"

#include "GraphGenerator/IOFiles/KMLWriter.h"
#include "Graph/Graph.h"

#include <iostream>
#include <string>

 class EdgeFactory
 {
 private:
 	OGRGeometry *poGeometry;
	OGRGeometry *neighborGeometry;
	int idEdge;
	int idNode;
 public:
 	EdgeFactory();
 	~EdgeFactory();
 
 	/* data */
 	void EdgeGenerator(Graph * graph);
 };
 #endif
