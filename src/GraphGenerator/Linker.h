/*
 * Linker.h
 *
 *  Created on: 4 juil. 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *  Edit on: feb 2022
 * 	Author: Thomas Guyet, Inria (thomas.guyet@inria.fr)
 */

#ifndef LINKER_H_
#define LINKER_H_

class MetaData;

#include <map>
#include <string>

using namespace std;

/**
 * Default name of the attribute that is to used as an identifier of a shape in a shapefile
 */
#define DEFAULT_ID "id"

/**
 * Shapes types
 */
enum GenericShapeTypes {NO_DATA, OBJECT, OBSTACLE};

/**
 * \author Antoine Despres
 * \year 2012
 * \brief
 */
class Linker {
private:
	map<string, GenericShapeTypes> links;
public:
	Linker();
	virtual ~Linker();

	void reference(const string& ref, GenericShapeTypes type){links.insert(pair<string,GenericShapeTypes>(ref,type));}

	/**
	 * Return the type of shape for the MetaData of a shape
	 * \see GenericShapeTypes
	 * \see MetaData
	 */
	virtual GenericShapeTypes findType(const MetaData& meta) const;

	virtual string getRefField() const =0;
	virtual string getIdField() const {return DEFAULT_ID;}
};

/**
 * All shape in the file using this linker are of one type, by default OBJECT
 */
class SimpleLinker : public Linker {
private:
	string refField;
	string idField;

	GenericShapeTypes type;
public:
	SimpleLinker(string refField, string idField, GenericShapeTypes type);
	virtual ~SimpleLinker();

	virtual GenericShapeTypes findType(const MetaData& meta) const {return type;}

	virtual string getRefField() const {return refField;}
	virtual string getIdField() const {return idField;}
};

#endif /* LINKER_H_ */
