/*
 * main.cpp
 *
 *  Created on: 22 juin 2012
 *      Author: Antoine Despres (antoine.despres@yahoo.fr)
 *      Author: Thomas Guyet (thomas.guyet@agrocampus-ouest.fr)
 *
 * <ul>
 * <li>20/09/2012 : refonte du passage des paramètres</li>
 * </ul>
 */

#include <list>
#include <string>
#include <iostream>

#include "../geoSpan/MinerLauncher.h"
#include "../geoSpan/Pattern.h"
#include "../geoSpan/MinerParameters.h"
#include "../Graph/Node.h"


using namespace std;


void usage(char* c){

	if( c!=0 ) {
		cerr<<"Error : "<<c<<" is an invalid argument!"<<endl;
	}
	cout << "Usage is -g graph_file\" [-o output_dir] [-m method] [-md %%d] [-t threshold] [-inc increment] [-l lambda] [-v] "<<endl;
	cout << "methods (default geoAdapt):\n";
	cout << "\t-classic : provides a classic mining, takes one parameter -t\n";
	cout << "\t-geoAdapt : provides a mining that uses an adaptative \n"
			"threshold and spatial constraints, takes three parameters -inc, -t and -l\n";
	cout << "\noptions:\n";
	cout << "\t-md : max depth (default none)\n";
	cout << "\t-t : threshold or max threshold (default 20)\n";
	cout << "\t-inc : incrementation multiplier (>0) (default 1)\n";
	cout << "\t-l : spatial constraint (>0) (default 300)\n";
	cout << "\t-o : output directory (default local directory)\n";
	cout << "\t-v : verbose\n";
}


int main(int argc, char **argv) {

	if (argc < 3) {
		usage(0);
		exit(0);
	} else { // if we got enough parameters...
		string graph, outPath, method="geoAdapt";
		float inc=1, threshold=20, maxD=300, maxdepth=0;
		bool verbose=false;
		for (int i = 1; i < argc; i++) { //0 is the path of the program
			if (i != argc){ // Check that we haven't finished parsing already
				if (string(argv[i]) == "-g" || string(argv[i]) == "-graph") {//path to the graph
					graph = argv[++i];
				} else if (string(argv[i]) == "-o" || string(argv[i]) == "-out") {
					outPath = argv[++i];
				} else if (string(argv[i]) == "-m" || string(argv[i]) == "-method") {
					method = argv[++i];
				} else if (string(argv[i]) == "-md") {
					maxdepth = atoi(argv[++i]);
				} else if (string(argv[i]) == "-t") {
					threshold = atof(argv[++i]);
				} else if (string(argv[i]) == "-inc") {
					inc = atof(argv[++i]);
				} else if (string(argv[i]) == "-l") {
					maxD = atof(argv[++i]);
				} else if (string(argv[i]) == "-v" || string(argv[i]) == "-verbose") {
					verbose=true;
				} else {
					usage(argv[i]);
					exit(0);
				}
			}
		}

		MinerParameters* p=NULL;

		if(method=="classic"){
			p=new NoParameters(threshold);
		} else if(method=="geoAdapt") {
			p= new compactQuadIncNodeParameters(threshold, inc, maxD);
		} else {
			usage(0);
			exit(0);
		}

		p->setMaxDepth(maxdepth);

		MinerLauncher::launchMiner(graph, outPath, p, verbose);

		delete p;

		return 0;
	}
}
